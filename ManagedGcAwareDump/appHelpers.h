#pragma once

#include <string>
#include "unique_handle.h"

const DWORD CLR_NOTIFICATION_EXCEPTION_CODE = 0xE0444143;

/* Return current directory with ending slash	*/
std::wstring getCurrentDirectory();

/* Extract directory path from file path. Returns ending slash */
std::wstring getDirectoryPath(const std::wstring& filePath);

std::wstring getNameWithoutExtension(std::wstring const& fileName);
bool fileExits(std::wstring const& filePath);

inline HMODULE getMainModuleHandle()
{
	return GetModuleHandle(nullptr);
}

template <class RetT>
RetT findModuleData(DWORD procId, std::function<bool(MODULEENTRY32 const&)> predicate, std::function<RetT(MODULEENTRY32 const&)> selector)
{
	unique_handle hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, procId);
	if (!hSnapshot.isValid())
	{
		return 0;
	}

	MODULEENTRY32 modEntry;
	modEntry.dwSize = sizeof(MODULEENTRY32);

	if (!Module32First(hSnapshot.get(), &modEntry))
	{
		return 0;
	}

	do
	{
		if (predicate(modEntry))
		{
			return selector(modEntry);
		}
	} while (Module32Next(hSnapshot.get(), &modEntry));

	return 0;
}

inline wchar_t const* GetCurrentArchitectureStr()
{
#ifdef _WIN64
	return L"X64";
#else
	return L"X86";
#endif
}

class AdvRetCode
{
public:
	AdvRetCode() :AdvRetCode(0){}

	AdvRetCode(int errCode, HRESULT advErrCode = 0) :
		_errCode(errCode), _advancedErrCode(advErrCode)
	{

	}

	bool isError() const{ return this->_errCode != 0; }
	int _errCode;
	HRESULT _advancedErrCode;

};

inline std::wostream& operator<< (std::wostream& os, const AdvRetCode& advRetCode)
{
	os << L"Error code: " << advRetCode._errCode;
	if (advRetCode._advancedErrCode != 0)
	{
		os << L"[" << std::hex << advRetCode._advancedErrCode << std::dec << L"]";
	}

	return os;
}