#include "stdafx.h"
#include "DebugClrDataTarget.h"
#include "DebugClient.h"

HRESULT DebugClrDataTarget::QueryInterface(IID const& riid, void** ppvObject)
{
	bool success = false;

	if (IsEqualIID(riid, IID_IUnknown))
	{
		*ppvObject = this;
		success = true;
	}
	else if (IsEqualIID(riid, __uuidof(ICLRDataTarget)))
	{
		*ppvObject = static_cast<ICLRDataTarget*>(this);
		success = true;
	}
	else if (IsEqualIID(riid, __uuidof(ICLRDataTarget2)))
	{
		*ppvObject = static_cast<ICLRDataTarget2*>(this);
		success = true;
	}

	if (success)
	{
		this->AddRef();
		return S_OK;
	}
	else
	{
		*ppvObject = nullptr;
		return E_NOINTERFACE;
	}
}

ULONG DebugClrDataTarget::AddRef()
{
	return InterlockedIncrement(&this->_refCount);
}

ULONG DebugClrDataTarget::Release()
{
	LONG oldRef = InterlockedDecrement(&this->_refCount);
	if (this->_refCount == 0)
		delete this;

	return oldRef;
}

HRESULT DebugClrDataTarget::GetMachineType(ULONG32* machineType)
{
#if _WIN64
	*machineType = IMAGE_FILE_MACHINE_AMD64 ;
#else
	*machineType = IMAGE_FILE_MACHINE_I386;
#endif

	return S_OK;
}

HRESULT DebugClrDataTarget::GetPointerSize(ULONG32* pointerSize)
{
	*pointerSize = sizeof(void*);
	return S_OK;
}

HRESULT DebugClrDataTarget::GetImageBase(LPCWSTR imagePath, CLRDATA_ADDRESS* baseAddress)
{
	if (wcscmp(imagePath, L"clr.dll") == 0 || wcscmp(imagePath, L"mscorwks.dll") == 0)
	{
		*baseAddress = reinterpret_cast<CLRDATA_ADDRESS>(this->_coreLibBase);
		return S_OK;
	}

	return E_UNEXPECTED;
}

HRESULT DebugClrDataTarget::ReadVirtual(CLRDATA_ADDRESS address, BYTE* buffer, ULONG32 bytesRequested, ULONG32* bytesRead)
{
	ULONG bytesReadLoc = 0;
	HRESULT status = _debClient._debugDataSpaces->ReadVirtual(address, buffer, bytesRequested, &bytesReadLoc);
	*bytesRead = bytesReadLoc;
	return status;
}

HRESULT DebugClrDataTarget::WriteVirtual(CLRDATA_ADDRESS address, BYTE* buffer, ULONG32 bytesRequested, ULONG32* bytesWritten)
{
	auto status = this->_debClient._debugDataSpaces->WriteVirtual(address, buffer, bytesRequested, reinterpret_cast<PULONG>(bytesWritten));
	return status;
}

HRESULT DebugClrDataTarget::GetTLSValue(ULONG32 threadID, ULONG32 index, CLRDATA_ADDRESS* value)
{
	int a = 10;
	return E_NOTIMPL;
}

HRESULT DebugClrDataTarget::SetTLSValue(ULONG32 threadID, ULONG32 index, CLRDATA_ADDRESS value)
{
	int a = 10;
	return E_NOTIMPL;
}

HRESULT DebugClrDataTarget::GetCurrentThreadID(ULONG32* threadID)
{
	int a = 10;
	return E_NOTIMPL;
}

HRESULT DebugClrDataTarget::GetThreadContext(ULONG32 threadID, ULONG32 contextFlags, ULONG32 contextSize, BYTE* context)
{
	int a = 10;
	return E_NOTIMPL;
}

HRESULT DebugClrDataTarget::SetThreadContext(ULONG32 threadID, ULONG32 contextSize, BYTE* context)
{
	int a = 10;
	return E_NOTIMPL;
}

HRESULT DebugClrDataTarget::Request(ULONG32 reqCode, ULONG32 inBufferSize, BYTE* inBuffer, ULONG32 outBufferSize, BYTE* outBuffer)
{
	int a = 10;
	return E_NOTIMPL;
}

HRESULT DebugClrDataTarget::AllocVirtual(CLRDATA_ADDRESS addr, ULONG32 size, ULONG32 typeFlags, ULONG32 protectFlags, CLRDATA_ADDRESS* virt)
{
	ULONG64 procHandle;
	if (this->_debClient._debugSystemObjects->GetCurrentProcessHandle(&procHandle) != S_OK)
	{
		return E_FAIL;
	}

	HANDLE hProcess = reinterpret_cast<HANDLE>(procHandle);
	*virt = reinterpret_cast<CLRDATA_ADDRESS>(VirtualAllocEx(hProcess, reinterpret_cast<void*>(addr), size, typeFlags, protectFlags));

	return *virt == 0 ? E_FAIL : S_OK;
}

HRESULT DebugClrDataTarget::FreeVirtual(CLRDATA_ADDRESS addr, ULONG32 size, ULONG32 typeFlags)
{
	int a = 10;
	return E_NOTIMPL;
}