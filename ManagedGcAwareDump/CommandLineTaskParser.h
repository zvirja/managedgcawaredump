#pragma once
#include <string>
#include <map>

class CommandLineTaskParser
{
public:
	CommandLineTaskParser(int argc, _TCHAR* argv[]) :
		_argc(argc), _argv(argv), _parsedProcId(0), _parsedProcName(), _forceGcBeforeCollection(false), _gcBeforeCollectionGenNum(-1), 
		 _waitForPendingFinalizers(false)
	{
	}

	~CommandLineTaskParser()
	{
	};

	bool parse();

	ULONG getProcId() const
	{
		return this->_parsedProcId;
	}

	std::wstring const& getProcName() const
	{
		return this->_parsedProcName;
	}

	bool resolveByProcId() const
	{
		return this->_parsedProcId != 0;
	}

	//Returns array from 0 to 3, where each index value means number of dumps, should be collected for particular gen.
	std::array<int, 3> getGenCollectionTasksCopy() const
	{
		return this->_genCollectionTasks;
	}

	bool getForceGcBeforeCollection() const
	{
		return this->_forceGcBeforeCollection;
	}

	int getGcBeforeCollectionGenNum() const
	{
		return this->_gcBeforeCollectionGenNum;
	}

	bool waitForPendingFinalizers() const
	{
		return this->_waitForPendingFinalizers;
	}

private:
	int _argc;
	_TCHAR** _argv;
	std::wstring _parsedProcName;
	ULONG _parsedProcId;
	std::array<int, 3> _genCollectionTasks;
	bool _forceGcBeforeCollection;
	int _gcBeforeCollectionGenNum;
	bool _waitForPendingFinalizers;

	void printHelp();
	int extractKeys();
	bool processKey(std::wstring const& token);
};