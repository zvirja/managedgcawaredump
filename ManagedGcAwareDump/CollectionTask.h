#pragma once
#include "CommandLineTaskParser.h"
#include "FileVersionHelper.h"

class CollectionTask
{
public:
	CollectionTask(CommandLineTaskParser const& cmdParser) :
		_cmdParser(cmdParser), _pid(0), _genTasks(cmdParser.getGenCollectionTasksCopy()), _forceGcBeforeCollection(cmdParser.getForceGcBeforeCollection()),
		_waitForPendingFinalizers(cmdParser.waitForPendingFinalizers()), _gcToForceBeforeCollection(cmdParser.getGcBeforeCollectionGenNum())
	{
		this->_isGenSpecificTask = !this->allGensAreTraced();
	}

	~CollectionTask();

	bool resolveAndValidateTask();

	CommandLineTaskParser const& getCmdParser() const
	{
		return this->_cmdParser;
	}

	ULONG getPid() const
	{
		return this->_pid;
	}

	PVOID getCoreDllAddress() const
	{
		return this->_coreDllAddress;
	}

	std::wstring const& getCoreDllPath() const
	{
		return this->_coreDllPath;
	}

	std::wstring const& getProcNameWithoutExt() const
	{
		return this->_procNameWithoutExt;
	}

	FileVersionInfo const& getCoreDllVersionInfo() const
	{
		return this->_coreDllFileVerHelper->getFileVersionInfo();
	}

	std::array<int, 3>& getGenTasks()
	{
		return this->_genTasks;
	}

	std::array<int, 3> const& getGenTasks() const
	{
		return this->_genTasks;
	}

	bool allGensAreTraced() const
	{
		return this->_genTasks.at(0) == 0 && this->_genTasks.at(1) == 0 && this->_genTasks.at(2) == 0;
	}

	bool isGenSpecificTask() const
	{
		return this->_isGenSpecificTask;
	}

	bool shouldForceGcBeforeCollection() const
	{
		return this->_forceGcBeforeCollection;
	}

	bool waitForPendingFinalizers() const
	{
		return this->_waitForPendingFinalizers;
	}

	int getGcToForceBeforeCollectionNum() const
	{
		return this->_gcToForceBeforeCollection;
	}

private:
	CommandLineTaskParser const& _cmdParser;
	ULONG _pid;
	std::wstring _procNameWithoutExt;
	PVOID _coreDllAddress;
	std::wstring _coreDllPath;
	std::unique_ptr<FileVersionHelper> _coreDllFileVerHelper;
	std::array<int, 3> _genTasks;
	bool _isGenSpecificTask;
	bool _forceGcBeforeCollection;
	bool _waitForPendingFinalizers;
	int _gcToForceBeforeCollection;

	bool ensureProcessIdAndName();
	bool validateBitness();
	bool resolveCoreClrDll();
	int bitnessMatch(unique_handle const& hTargetProc);
	bool initCoreDllVersionInfo();
	bool ensureDotNetVersionForOnGc();
	bool validateFlagsConsistency();
	bool validateRedistrInstalled();
};