#pragma once
#include "DebugClient.h"

class DebugEventCallbacks : public IDebugEventCallbacks
{
public:
	DebugEventCallbacks(DebugClient& debClient) : _refCount(1), _debugClient(debClient)
	{
	}

	virtual ~DebugEventCallbacks();
	DebugEventCallbacks(DebugEventCallbacks&) = delete;
	DebugEventCallbacks& operator=(DebugEventCallbacks&) = delete;


	HRESULT STDMETHODCALLTYPE QueryInterface(IID const& InterfaceId, PVOID* Interface) override;
	ULONG STDMETHODCALLTYPE AddRef() override;
	ULONG STDMETHODCALLTYPE Release() override;
	HRESULT STDMETHODCALLTYPE GetInterestMask(PULONG Mask) override;
	HRESULT STDMETHODCALLTYPE Breakpoint(PDEBUG_BREAKPOINT Bp) override;
	HRESULT STDMETHODCALLTYPE Exception(PEXCEPTION_RECORD64 Exception, ULONG FirstChance) override;
	HRESULT STDMETHODCALLTYPE CreateThread(ULONG64 Handle, ULONG64 DataOffset, ULONG64 StartOffset) override;
	HRESULT STDMETHODCALLTYPE ExitThread(ULONG ExitCode) override;
	HRESULT STDMETHODCALLTYPE CreateProcessW(ULONG64 ImageFileHandle, ULONG64 Handle, ULONG64 BaseOffset, ULONG ModuleSize, PCSTR ModuleName, PCSTR ImageName, ULONG CheckSum, ULONG TimeDateStamp, ULONG64 InitialThreadHandle, ULONG64 ThreadDataOffset, ULONG64 StartOffset) override;
	HRESULT STDMETHODCALLTYPE ExitProcess(ULONG ExitCode) override;
	HRESULT STDMETHODCALLTYPE LoadModule(ULONG64 ImageFileHandle, ULONG64 BaseOffset, ULONG ModuleSize, PCSTR ModuleName, PCSTR ImageName, ULONG CheckSum, ULONG TimeDateStamp) override;
	HRESULT STDMETHODCALLTYPE UnloadModule(PCSTR ImageBaseName, ULONG64 BaseOffset) override;
	HRESULT STDMETHODCALLTYPE SystemError(ULONG Error, ULONG Level) override;
	HRESULT STDMETHODCALLTYPE SessionStatus(ULONG Status) override;
	HRESULT STDMETHODCALLTYPE ChangeDebuggeeState(ULONG Flags, ULONG64 Argument) override;
	HRESULT STDMETHODCALLTYPE ChangeEngineState(ULONG Flags, ULONG64 Argument) override;
	HRESULT STDMETHODCALLTYPE ChangeSymbolState(ULONG Flags, ULONG64 Argument) override;
private:
	LONG _refCount;
	DebugClient& _debugClient;
};