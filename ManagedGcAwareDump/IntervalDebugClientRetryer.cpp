#include "stdafx.h"
#include "IntervalDebugClientRetryer.h"
#include "DebugClient.h"

using namespace std;

int IntervalDebugClientRetryer::TryUntilSuccessOrFail()
{
	if (!this->startBgWakeupThread())
	{
		return -10;
	}

	ScopeGuard bgThreadRequired([this]()
		{
			_bgThreadRequired.store(false, memory_order_release);
		});

	HRESULT status;
	int tryNum = 0;
	do
	{
		status = this->_debClient._debugControl->SetExecutionStatus(DEBUG_STATUS_GO);
		//break, because unable to resume target
		if (status != S_OK)
		{
			return -1;
		}

		//schedule our next interrupt if it isn't pending
		if (this->_nextFireTime.load(memory_order_acquire) == 0)
		{
			ULONGLONG now = GetTickCount64();
			this->_nextFireTime.store(now + this->_delay, memory_order_release);
		}

		{
			auto interruptableScope = this->_debClient.enterInterruptableState();
			//wait for delay * 2 - protection from infinite sleep
			status = this->_debClient._debugControl->WaitForEvent(0, this->_delay * 8);
		}

		//target exited
		if (status == E_PENDING)
		{
			this->_debClient.setExitReason(ExitReason::TargedExited);
			return 1;
		}

		//failed
		if (status == E_FAIL)
		{
			return -2;
		}

		//timeout expired. Retry
		if (status == S_FALSE)
		{
			continue;
		}

		if (status == S_OK)
		{
			if (this->_debClient.getShouldExit())
			{
				return 2;
			}

			//Check whether we could successfully execute condition this time
			if (this->_func(++tryNum) == true)
			{
				return 0;
			}

			continue;
		}

		//If not handled above, break.
		break;
	}
	while (true);

	return -3;
}

bool IntervalDebugClientRetryer::startBgWakeupThread()
{
	thread t(&IntervalDebugClientRetryer::bgWakeupThreadWorkLoop, this);
	t.detach();

	return true;
}

void IntervalDebugClientRetryer::bgWakeupThreadWorkLoop()
{
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);

	while (this->_bgThreadRequired.load(memory_order_acquire))
	{
		ULONGLONG nextFireTime = this->_nextFireTime.load(memory_order_acquire);

		if (nextFireTime != 0)
		{
			ULONGLONG now = GetTickCount64();
			if (now >= nextFireTime && nextFireTime != 0)
			{
				this->_nextFireTime.store(0, memory_order_release);
				this->_debClient.interruptIfNeeded();
			}
		}

		//Half of time, to get max delay in 3/2 delay.
		Sleep(this->_delay / 2);
	}
}