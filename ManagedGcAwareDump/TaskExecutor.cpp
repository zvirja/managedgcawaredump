#include "stdafx.h"
#include "TaskExecutor.h"
#include "GcCollectionRunnerAssemblyInfo.h"
#include "resource.h"
#include <Accctrl.h>
#include <Aclapi.h>

using namespace std;

TaskExecutor::~TaskExecutor()
{
}

void TaskExecutor::executeTaskAfterAttach()
{
#ifndef _GCRUNNERTEST
	if (!this->initializeDacInterface())
	{
		return;
	}
#endif

	if (this->_collectionTask.isGenSpecificTask())
	{
		if (this->initGcEventsListening_PFR(true))
		{
			this->collectDumpOnGcEvents();
		}
	}
	else if (this->_collectionTask.shouldForceGcBeforeCollection())
	{
		this->forceGcAndCollectDump();
	}
	else
	{
		this->createDumpFileWhenGcHeapIsValid();
	}
}

void TaskExecutor::executeTaskAfterDetach_PFR()
{
	if (!this->_gcTriggerAssemblyWasHosted)
	{
		return;
	}

#ifdef _WIN64
	DWORD funcOffset = GC_RUNNER_X64_UNLOAD_OFFSET;
#else
	DWORD funcOffset = GC_RUNNER_X86_UNLOAD_OFFSET;
#endif

	BYTE* unloadFuncAddr = this->_gcTriggerAssemblyBaseAddr + funcOffset;
	this->executeInTarget(reinterpret_cast<LPTHREAD_START_ROUTINE>(unloadFuncAddr), nullptr);
}

bool TaskExecutor::executeTaskBeforeAttach_PFR()
{
	if (!this->_collectionTask.shouldForceGcBeforeCollection())
	{
		return true;
	}

	int retCode = 0;
	AdvRetCode advRetCode = this->extractAssemblyFromResources();
	if (advRetCode.isError())
	{
		wcout << L"Unable to extract service assembly. Ensure that tool has write access to current folder. " << advRetCode << endl;
		return false;
	}

	if (!this->openTargetProcess())
	{
		wcout << L"Unable to obtain handle to the target process." << endl;
		return false;
	}

	advRetCode = this->hostGcRebuildAssembly();
	if (advRetCode.isError())
	{
		wcout << L"Unable to host the GC triggering assembly inside the target process. " << advRetCode << endl;
		return false;
	}

	return true;
}

bool TaskExecutor::initializeDacInterface()
{
	using namespace placeholders;

	DWORD retCode = this->_clrManager.initializeBasicInterfaces();
	if (retCode == 0 && this->_collectionTask.isGenSpecificTask())
	{
		retCode = this->_clrManager.initializeExtendedInterfaces(bind(&TaskExecutor::onGcEvent, this, _1));
	}

	if (retCode != 0)
	{
		wcout << L"Unable to create DAC interface. Ensure that target process is a valid .NET process. Error code: " << hex << retCode << dec << endl;
		return false;
	}

	return true;
}

void TaskExecutor::createDumpFileWhenGcHeapIsValid()
{
	//Created or failed. Exit anyway
	if (this->tryCreateDumpIfHeapValid(0))
	{
		return;
	}

	wcout << L"GC heap is not in valid state. Tool will await until heap becomes valid..." << endl;

	int retCode = this->_debugClient.tryWithInterval([this](int tryNum)
	{
		return tryCreateDumpIfHeapValid(tryNum);
	}, 250);

	if (retCode < 0)
	{
		wcout << L"Unable to properly wait until GC heap becomes valid. Error code: " << retCode << endl;
		this->_debugClient.setExitReason(ExitReason::Failure);
	}
}

/**
	Create dump file if heap is valid.
	Function returns true if successfully created or in case of failure.
	In case of insuccessfull attempt false is returned.
	*/
bool TaskExecutor::tryCreateDumpIfHeapValid(int tryNumber)
{
	int gcHeapIsValid = this->_clrManager.isGcHeapValid();
	//gcHeapIsValid = 0;
	if (gcHeapIsValid < 0)
	{
		wcout << L"Unable to check whether GC heap is valid. Return code: " << gcHeapIsValid << endl;
		return true;
	}

	if (gcHeapIsValid > 0)
	{
		wstring dumpPrefix;
		if (this->_collectionTask.shouldForceGcBeforeCollection())
		{
			wstringstream dumpName;
			dumpName << this->_collectionTask.getProcNameWithoutExt();
			dumpName << L"_FGC";
			if (this->_collectionTask.getGcToForceBeforeCollectionNum() > -1)
			{
				dumpName << _collectionTask.getGcToForceBeforeCollectionNum();
			}

			if (this->_collectionTask.waitForPendingFinalizers())
			{
				dumpName << L"_WPF";
			}

			dumpPrefix = dumpName.str();
		}

		if (this->_debugClient.createDumpFile(dumpPrefix.size() > 0 ? dumpPrefix : this->_collectionTask.getProcNameWithoutExt()) && tryNumber > 0)
		{
			wcout << L"Dump file was created on attempt #" << tryNumber << endl;
		}
		return true;
	}

	return false;
}

bool TaskExecutor::initGcEventsListening_PFR(bool updateClr)
{
	using namespace placeholders;

	bool success = this->_debugClient.enableClrNotificationExceptions(std::bind(&TaskExecutor::onNotificationException, this, _1, _2));
	if (!success)
	{
		wcout << "Unable to initialize listening for CLR Notification exceptions." << endl;
		return false;
	}

	if (updateClr)
	{
		if (!this->setGcNotificationMask_PFR(this->getNewGcNotificationsMask()))
		{
			return false;
		}
	}

	return true;
}

void TaskExecutor::collectDumpOnGcEvents()
{
	//everything is initialized. Just let events happen.
	this->_debugClient.executeTargetUntilExitRequested();

	//sometimes it returns in unregular cases (e.g. Ctrl+C).
	this->_clrManager.resetGcNotifications();
}


void TaskExecutor::onNotificationException(PEXCEPTION_RECORD64 exception, ULONG isFirstChance)
{
	//Handle our dummy notification exception
	if (this->_customGcNotificationExceptionIsExpected)
	{
		//our custom notification exception
		if (exception->NumberParameters == 1 && (exception->ExceptionInformation[0] & 0xffffffff) == 0xdeadf00d)
		{
			this->onRequestedGcEventFinished();
		}
		else
		{
			wcout << L"Unknown CLR notification exception. Exiting..." << endl;
			this->_debugClient.setExitReason(ExitReason::Failure);
		}

		return;
	}

	if (!this->_clrManager.translateNotificationException(exception, isFirstChance))
	{
		wcout << L"Unable to process notification exception from CLR. Exiting..." << endl;
		this->_debugClient.setExitReason(ExitReason::Failure);
	}
}

void TaskExecutor::onGcEvent(ULONG64 gcEventInfo)
{
	//Disable all GC events before. Need to do that, otherwise mask might be updated wrongly
	if (!this->setGcNotificationMask_PFR(0))
	{
		return;
	}

	int currentGen = this->getGetFromNotification(gcEventInfo);
	if (currentGen < 0)
	{
		wcout << L"Unable to parse generation from GEN notification." << endl;
		this->_debugClient.setExitReason(ExitReason::Failure);
		return;
	}

	wcout << "Generation " << currentGen << " is about to be collected." << endl;
	if (this->_collectionTask.getGenTasks()[currentGen] > 0)
	{
		this->_collectionTask.getGenTasks()[currentGen]--;

		//make prefix
		wstringstream ss;
		ss << this->_collectionTask.getProcNameWithoutExt();
		ss << "_GEN";
		ss << currentGen;

		if (!this->_debugClient.createDumpFile(ss.str()))
		{
			this->_debugClient.setExitReason(ExitReason::Failure);
		}
	}
	else
	{
		wcout << " Skipping.." << endl;
	}

	if (this->_collectionTask.allGensAreTraced())
	{
		wcout << endl << L"Dump count reached. Exiting." << endl;
		this->_debugClient.setExitReason(ExitReason::TaskCompleted);
	}
	else
	{
		if (!this->setGcNotificationMask_PFR(this->getNewGcNotificationsMask()))
		{
			return;
		}

		wcout << L"Waiting for the next GC notification..." << endl << endl;
	}

	return;
}

DWORD32 TaskExecutor::getNewGcNotificationsMask()
{
	DWORD32 notifMask = 0;
	auto& genTasks = this->_collectionTask.getGenTasks();
	if (genTasks[0] > 0)
	{
		notifMask |= 1;
	}
	if (genTasks[1] > 0)
	{
		notifMask |= 2;
	}
	if (genTasks[2] > 0)
	{
		notifMask |= 4;
	}

	return notifMask;
}

int TaskExecutor::getGetFromNotification(ULONG64 gcEventNotification)
{
	if ((gcEventNotification & (1ull << 32)) != 0)
	{
		return 0;
	}
	if ((gcEventNotification & (2ull << 32)) != 0)
	{
		return 1;
	}
	if ((gcEventNotification & (4ull << 32)) != 0)
	{
		return 2;
	}

	return -1;
}

bool TaskExecutor::setGcNotificationMask_PFR(DWORD32 mask)
{
	if (!this->_clrManager.setGcNotificationMode(mask))
	{
		this->_debugClient.setExitReason(ExitReason::Failure);
		wcout << "Unable to alter CLR GC notifications. Exiting..." << endl;
		return false;
	}

	return true;
}

void TaskExecutor::onRequestedGcEventFinished()
{
	if (this->_debugClient.getExitReason() == ExitReason::NoExit)
	{
		this->_debugClient.setExitReason(ExitReason::SystemCustomGcHappened);
	}
}

AdvRetCode TaskExecutor::hostGcRebuildAssembly()
{
	wcout << L"Preparing for remote GC. Hosting the service assembly ..." << endl;

	LPTHREAD_START_ROUTINE startRoutine = reinterpret_cast<LPTHREAD_START_ROUTINE>(GetProcAddress(GetModuleHandle(L"kernel32.dll"), "LoadLibraryW"));
	if (startRoutine == nullptr)
	{
		return AdvRetCode(-1, GetLastError());
	}

	size_t dllPathSize = (this->_gcTriggerAssemblyPath.size() + 1) * sizeof(wchar_t);

	LPVOID argBuffer = VirtualAllocEx(_hTargetProc.get(), nullptr, dllPathSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (argBuffer == nullptr)
	{
		return AdvRetCode(-2, GetLastError());
	}

	ScopeGuard memDeallocator([argBuffer, dllPathSize, this]()
	{
		VirtualFreeEx(_hTargetProc.get(), argBuffer, dllPathSize, MEM_FREE);
	});

	SIZE_T writtenBytes;
	int n = WriteProcessMemory(_hTargetProc.get(), argBuffer, _gcTriggerAssemblyPath.c_str(), dllPathSize, &writtenBytes);
	if (n == 0)
	{
		return AdvRetCode(-3, GetLastError());
	}

	if (grandEveryoneAccessToAssembly(this->_gcTriggerAssemblyPath) != 0)
	{
		return AdvRetCode(-4, GetLastError());
	}


	AdvRetCode injectExitCode = this->executeInTarget(startRoutine, argBuffer);

#ifdef _ADVLOG
	wcout << "Assembly inject ret code: " << injectExitCode << endl;
#endif

	if (injectExitCode.isError())
	{
		return AdvRetCode(-6, injectExitCode._advancedErrCode);
	}

	//validate that assembly is loaded

	wstring lowercasedDllPath;
	lowercasedDllPath.resize(_gcTriggerAssemblyPath.size());

	::transform(_gcTriggerAssemblyPath.begin(), _gcTriggerAssemblyPath.end(), lowercasedDllPath.begin(), ::towlower);

	BYTE* modBaseAddr = findModuleData<BYTE*>(this->_collectionTask.getPid(),
		[&lowercasedDllPath](MODULEENTRY32 const& modEntry)
	{
		wstring modPath(modEntry.szExePath);
		::transform(modPath.begin(), modPath.end(), modPath.begin(), ::towlower);

		return modPath.compare(lowercasedDllPath) == 0;
	}, [](MODULEENTRY32 const& modEntry)
	{
		return modEntry.modBaseAddr;
	});

	if (modBaseAddr != nullptr)
	{
		this->_gcTriggerAssemblyBaseAddr = modBaseAddr;
		this->_gcTriggerAssemblyWasHosted = true;
		wcout << L"The service assembly was successfully hosted." << endl;
	}

	return modBaseAddr != nullptr ? AdvRetCode() : AdvRetCode(-5);
}

int TaskExecutor::grandEveryoneAccessToAssembly(std::wstring const& filePath)
{
	PSID pEveryoneSID = NULL;
	PACL pACL = NULL;
	EXPLICIT_ACCESS ea[1];
	SID_IDENTIFIER_AUTHORITY SIDAuthWorld = SECURITY_WORLD_SID_AUTHORITY;

	// Create a well-known SID for the Everyone group.
	AllocateAndInitializeSid(&SIDAuthWorld, 1,
		SECURITY_WORLD_RID,
		0, 0, 0, 0, 0, 0, 0,
		&pEveryoneSID);

	// Initialize an EXPLICIT_ACCESS structure for an ACE.
	// The ACE will allow Everyone read access to the key.
	ZeroMemory(&ea, 1 * sizeof(EXPLICIT_ACCESS));
	ea[0].grfAccessPermissions = 0xFFFFFFFF;
	ea[0].grfAccessMode = GRANT_ACCESS;
	ea[0].grfInheritance = NO_INHERITANCE;
	ea[0].Trustee.TrusteeForm = TRUSTEE_IS_SID;
	ea[0].Trustee.TrusteeType = TRUSTEE_IS_WELL_KNOWN_GROUP;
	ea[0].Trustee.ptstrName = (LPTSTR)pEveryoneSID;

	// Create a new ACL that contains the new ACEs.
	SetEntriesInAcl(1, ea, NULL, &pACL);

	// Initialize a security descriptor.  
	PSECURITY_DESCRIPTOR pSD = (PSECURITY_DESCRIPTOR)LocalAlloc(LPTR,
		SECURITY_DESCRIPTOR_MIN_LENGTH);

	InitializeSecurityDescriptor(pSD, SECURITY_DESCRIPTOR_REVISION);

	// Add the ACL to the security descriptor. 
	SetSecurityDescriptorDacl(pSD,
		TRUE,     // bDaclPresent flag   
		pACL,
		FALSE);   // not a default DACL 


	//Change the security attributes
	SetFileSecurity(filePath.c_str(), DACL_SECURITY_INFORMATION, pSD);

	if (pEveryoneSID)
		FreeSid(pEveryoneSID);
	if (pACL)
		LocalFree(pACL);
	if (pSD)
		LocalFree(pSD);

	return 0;
}

bool TaskExecutor::openTargetProcess()
{
	this->_hTargetProc = OpenProcess(PROCESS_ALL_ACCESS, FALSE, this->_collectionTask.getPid());
	return this->_hTargetProc.isValid();
}

void TaskExecutor::forceGcAndCollectDump()
{
#ifndef _GCRUNNERTEST
	if (!this->initGcEventsListening_PFR(false))
	{
		return;
	}
#endif

	this->_customGcNotificationExceptionIsExpected = true;

#ifdef _WIN64
	DWORD funcOffset = GC_RUNNER_X64_FORCEGC_OFFSET;
#else
	DWORD funcOffset = GC_RUNNER_X86_FORCEGC_OFFSET;
#endif

	BYTE* remoteRebuildAddr = this->_gcTriggerAssemblyBaseAddr + funcOffset;

	DWORD32 arg = 0;
	int genToForce = this->_collectionTask.getGcToForceBeforeCollectionNum();
	if (genToForce > -1)
	{
		arg |= (genToForce & 3);
		arg |= 0x10;
	}

	if (this->_collectionTask.waitForPendingFinalizers())
	{
		arg |= 0x100;
	}

	this->executeInTarget(reinterpret_cast<LPTHREAD_START_ROUTINE>(remoteRebuildAddr), reinterpret_cast<PVOID>(arg), false);

	wcout << L"Remote GC has been queued. Waiting until is finished ..." << endl;

#ifdef _GCRUNNERTEST
	return;
#endif

	this->_debugClient.executeTargetUntilExitRequested();

	//we are here. Which means that rather exit or NotificationException happened.
	if (_debugClient.getExitReason() == SystemCustomGcHappened)
	{
		//reset exit reason
		_debugClient.setExitReason(ExitReason::NoExit);
		wcout << L"Remote GC has been finished." << endl;
		this->createDumpFileWhenGcHeapIsValid();
	}
}

AdvRetCode TaskExecutor::executeInTarget(LPTHREAD_START_ROUTINE routineAddr, LPVOID arg, bool waitForExit)
{
	HANDLE hRemoteThreadRaw = nullptr;

	//Means we tried to use the regular way and failed
	if (this->_rtlCreateUserThreadProc == nullptr)
	{
		hRemoteThreadRaw = CreateRemoteThread(_hTargetProc.get(), nullptr, 0, routineAddr, arg, 0, nullptr);
	}

	if (hRemoteThreadRaw == nullptr)
	{
		//plan B. Using undocumented API.
		if (this->_rtlCreateUserThreadProc == nullptr)
		{
			HMODULE ntdllLib = LoadLibrary(L"ntdll.dll");
			_rtlCreateUserThreadProc = reinterpret_cast<_RtlCreateUserThread>(GetProcAddress(ntdllLib, "RtlCreateUserThread"));
		}

		if (this->_rtlCreateUserThreadProc == nullptr)
		{
			return AdvRetCode(200, GetLastError());
		}

		CLIENT_ID dClientId;
		NTSTATUS retCode = this->_rtlCreateUserThreadProc(_hTargetProc.get(), nullptr, FALSE, 0, nullptr, nullptr, routineAddr, arg, &hRemoteThreadRaw, &dClientId);

		if (retCode != 0)
		{
			return AdvRetCode(201, retCode);
		}
	}

	unique_handle hRemoteThread(hRemoteThreadRaw);

	if (waitForExit)
	{
		WaitForSingleObject(hRemoteThread.get(), INFINITE);

		DWORD exitCode;
		if (GetExitCodeThread(hRemoteThread.get(), &exitCode))
		{
			return AdvRetCode(0, exitCode);
		}
	}

	return AdvRetCode(0, -1);
}

AdvRetCode TaskExecutor::extractAssemblyFromResources()
{
	wstringstream dllNameStream;
	dllNameStream << L"GcCollectionRunner_";
	dllNameStream << GetCurrentArchitectureStr();
	dllNameStream << L".dll";

	this->_gcTriggerAssemblyName = dllNameStream.str();

	WCHAR tempDirBuffer[MAX_PATH + 1];
	if (GetTempPath(MAX_PATH + 1, tempDirBuffer) == 0)
	{
		return  AdvRetCode(4, GetLastError());;
	}

	this->_gcTriggerAssemblyPath = wstring(tempDirBuffer) + _gcTriggerAssemblyName;

#ifdef _ADVLOG
	wcout << L"Path for the host assembly: " << this->_gcTriggerAssemblyPath << endl;
#endif

	//Check whether existing assembly is OK
	if (fileExits(this->_gcTriggerAssemblyPath))
	{
		{
			//validate by size
			unique_handle hFile = CreateFile(this->_gcTriggerAssemblyPath.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, nullptr);
			if (!hFile.isValid())
			{
				return  AdvRetCode(1, GetLastError());;
			}

#ifdef _WIN64
			DWORD etalonSize = GC_RUNNER_X64_SIZE;
#else
			DWORD etalonSize = GC_RUNNER_X86_SIZE;
#endif

			DWORD actualFileSize = GetFileSize(hFile.get(), nullptr);
			if (actualFileSize == INVALID_FILE_SIZE)
			{
				return  AdvRetCode(2, GetLastError());;
			}

			if (actualFileSize == etalonSize)
			{
				return AdvRetCode();
			}
		}

		//File exists and is invalid
		if (DeleteFile(this->_gcTriggerAssemblyPath.c_str()) != TRUE)
		{
			return  AdvRetCode(3, GetLastError());;
		}
	}

	return this->extractResourceToFile(IDR_GC_RUNNER, this->_gcTriggerAssemblyPath);
}

AdvRetCode TaskExecutor::extractResourceToFile(int resourceID, std::wstring const& outputFileName)
{
	try
	{
		HRSRC hResource = FindResource(nullptr, MAKEINTRESOURCE(resourceID), RT_RCDATA);
		if (hResource == nullptr)
		{
			return AdvRetCode(-1, GetLastError());
		}

		HGLOBAL hFileResource = LoadResource(nullptr, hResource);
		if (hFileResource == nullptr)
		{
			return AdvRetCode(-2, GetLastError());
		}

		void* lpFile = LockResource(hFileResource);
		if (lpFile == nullptr)
		{
			return AdvRetCode(-3, GetLastError());
		}

		DWORD dwSize = SizeofResource(nullptr, hResource);
		if (dwSize == 0)
		{
			return AdvRetCode(-4, GetLastError());
		}

		unique_handle hFile = CreateFile(outputFileName.c_str(), GENERIC_READ | GENERIC_WRITE, 0, nullptr, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
		if (!hFile.isValid())
		{
			return AdvRetCode(-5, GetLastError());
		}

		unique_handle hFilemap = CreateFileMapping(hFile.get(), nullptr, PAGE_READWRITE, 0, dwSize, nullptr);
		if (!hFilemap.isValid())
		{
			return AdvRetCode(-6, GetLastError());
		}

		void* lpBaseAddress = MapViewOfFile(hFilemap.get(), FILE_MAP_WRITE, 0, 0, 0);
		CopyMemory(lpBaseAddress, lpFile, dwSize);
		UnmapViewOfFile(lpBaseAddress);

		return 0;
	}
	catch (...)
	{
	}
	return AdvRetCode(-7);
}