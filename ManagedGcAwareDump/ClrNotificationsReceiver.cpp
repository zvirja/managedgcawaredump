#include "stdafx.h"
#include "ClrNotificationsReceiver.h"


HRESULT ClrNotificationsReceiver::QueryInterface(IID const& riid, void** ppvObject)
{
	bool success = false;
	if (IsEqualIID(riid, __uuidof(IUnknown)))
	{
		*ppvObject = this;
		success = true;
	}
	else if (IsEqualIID(riid, __uuidof(IXCLRDataExceptionNotification)))
	{
		*ppvObject = static_cast<IXCLRDataExceptionNotification*>(this);
		success = true;
	}
	else if (IsEqualIID(riid, __uuidof(IXCLRDataExceptionNotification2)))
	{
		*ppvObject = static_cast<IXCLRDataExceptionNotification2*>(this);
		success = true;
	}
	else if (IsEqualIID(riid, __uuidof(IXCLRDataExceptionNotification3)))
	{
		*ppvObject = static_cast<IXCLRDataExceptionNotification3*>(this);
		success = true;
	}

	if (success)
	{
		this->_refCount++;
		return S_OK;
	}

	*ppvObject = nullptr;
	return E_NOINTERFACE;
}

ULONG ClrNotificationsReceiver::AddRef()
{
	return InterlockedIncrement(&this->_refCount);
}

ULONG ClrNotificationsReceiver::Release()
{
	LONG oldRef = InterlockedDecrement(&this->_refCount);
	if (this->_refCount == 0)
		delete this;

	return oldRef;
}

HRESULT ClrNotificationsReceiver::OnGcEvent(ULONG64 eventInfo)
{
	this->_gcEventHandler(eventInfo);
	return S_OK;
}

ClrNotificationsReceiver::~ClrNotificationsReceiver()
{
}