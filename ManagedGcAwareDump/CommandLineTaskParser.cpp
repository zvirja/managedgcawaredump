#include "stdafx.h"
#include "CommandLineTaskParser.h"

using namespace std;

bool CommandLineTaskParser::parse()
{
	if (this->_argc == 1)
	{
		this->printHelp();
		return false;
	}

	if (this->_argc < 2)
	{
		this->printHelp();
		return false;
	}

	std::wstring lowercasedArg(this->_argv[1]);
	std::transform(lowercasedArg.begin(), lowercasedArg.end(), lowercasedArg.begin(), ::tolower);

	vector<wstring> helpKeywords{ L"/?", L"help", L"-h", L"--help" };

	if (find(helpKeywords.begin(), helpKeywords.end(), lowercasedArg) != helpKeywords.end())
	{
		this->printHelp();
		return false;
	}

	int lastProcessedArgc = this->extractKeys();
	if (lastProcessedArgc >= this->_argc || lastProcessedArgc < 0)
	{
		//No ProcNameId arg
		if (lastProcessedArgc == this->_argc)
		{
			wcout << "Target process name or ID is not specified." << endl;
		}
		wcout << endl << "Read help to get more information how to use tool." << endl << endl;
		return false;
	}

	std::wstring pidOrProcNameOrHelpAsk(this->_argv[lastProcessedArgc + 1]);
	

	try
	{
		this->_parsedProcId = std::stoi(pidOrProcNameOrHelpAsk);
	}
	catch (...)
	{
		this->_parsedProcId = 0;
	}

	//Wrong format or process name specified
	if (this->_parsedProcId <= 0)
	{
		this->_parsedProcName = move(pidOrProcNameOrHelpAsk);
		this->_parsedProcId = 0;
	}

	return true;
}

int CommandLineTaskParser::extractKeys()
{
	int i = 0;
	while (++i < this->_argc)
	{
		wstring currentParam(this->_argv[i]);
		if (currentParam.size() == 0)
		{
			return i - 1;
		}

		if (currentParam[0] != '/')
		{
			return i - 1;
		}

		if (!this->processKey(currentParam))
		{
			//Something went wrong. Return wrong value to display help message.
			return -1;
		}
	};

	//this point should be never reached in theory
	return i;
}

bool CommandLineTaskParser::processKey(std::wstring const& key)
{
	size_t keyS = key.size();
	wstring lowerKey = key;
	::transform(key.begin(), key.end(), lowerKey.begin(), ::towlower);

	if (keyS < 2)
	{
		return false;
	}

	// /g token
	if (lowerKey[1] == L'g')
	{
		int gen = 0;
		int numOfTimes = 1;

		// /g0 or /g0:12345
		if (!(keyS == 3 || keyS > 4))
		{
			wcout << "The " << key << " key is wrong." << endl;
			return false;
		}

		wchar_t genStr = key[2];
		switch (genStr)
		{
		case L'0': gen = 0;
			break;
		case L'1': gen = 1;
			break;
		case L'2': gen = 2;
			break;
			//Wrong value
		default:
			wcout << "The specified generation '" << key << "' appears to be invalid." << endl;
			return false;
		}

		//parse :12345 part
		if (keyS > 4)
		{
			try
			{
				numOfTimes = stoi(key.substr(4, wstring::npos));
			}
			catch (...)
			{
				wcout << "Unable to parse number in the " << key << " key." << endl;
				return false;
			}
		}

		if (numOfTimes <= 0)
		{
			wcout << "Unable to process the " << key << " key. Num of dumps should be greater than zero." << endl;
			return false;
		}

		this->_genCollectionTasks[gen] += numOfTimes;
		return true;
	}

	// /fgc token or /fgc:[012]
	if (keyS >= 4 && key.substr(0, 4) == L"/fgc")
	{
		if (this->_forceGcBeforeCollection)
		{
			wcout << L"The '" << key << L"' key could be specified only once!" << endl;
			return false;
		}

		this->_forceGcBeforeCollection = true;

		if (keyS > 4)
		{
			if (keyS != 6)
			{
				wcout << L"The '" << key << L"' appears to be invalid!" << endl;
				return false;
			}

			wchar_t genStr = key[5];
			switch (genStr)
			{
			case L'0': _gcBeforeCollectionGenNum = 0;
				break;
			case L'1': _gcBeforeCollectionGenNum = 1;
				break;
			case L'2': _gcBeforeCollectionGenNum = 2;
				break;
				//Wrong value
			default:
				wcout << "The specified generation '" << key << "' appears to be invalid." << endl;
				return false;
			}
		}

		return true;
	}

	// /wpf
	if (lowerKey.compare(L"/wpf") == 0)
	{
		this->_waitForPendingFinalizers = true;
		return true;
	}

	wcout << L"The " << key << " key is unknown." << endl;

	return false;
}

void CommandLineTaskParser::printHelp()
{
	wchar_t* msg = L"Tool allows to create a full memory dump of the target process at GC aware points of time. "
		L"The dump file is always gathered when GC heap is consistent (traversable). Tool support variety of options where to collect the dump file.\r\n\n"
		L"SYNTAX:\r\n"
		L"ManagedGcAwareDump [/g{0,1,2}[:N]] [/fgc[:{0,1,2}] [/wpf]] ProcessNameOrPID \r\n\n"
		L"FLAGS:\r\n"
		L"  If no flags are specified -- collect at first suitable moment.\r\n\n"
		L"  /g{0,1,2} -- Collect dump immediately _before_ specified gen collection.\r\n"
		L"     :N -- number of times dump should be collected.\r\n\n"
		L"  /fgc -- Force GC before dump collection. Incompatible with /g* flag.\r\n"
		L"     :{0,1,2} -- force 0-{val} gens collection (is an arg for GC.Collect(n)).\r\n\n"
		L"  /wpf -- Wait for pending finalizers after GC collection.\r\n"
		L"          Could be used if the /fgc flag is specified only.\r\n\n"
		L"EXAMPLES:\r\n\n"
		L"ManagedGcAwareDump 1234 -- Collect dump at first suitable moment of time.\r\n\n"
		L"ManagedGcAwareDump /fgc 1234 -- Force GC and collect dump at the end.\r\n\n"
		L"ManagedGcAwareDump /g0:2 /g2 1234 -- Collect dump file on Gen 0 collection (2 dump files) and one time on Gen 2 collection.";

	wcout << msg << endl;
}