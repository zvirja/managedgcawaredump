#pragma once

typedef struct
{
	DWORD Major;
	DWORD Minor;
	DWORD Build;
	DWORD Revision;
} FileVersionInfo;

class FileVersionHelper
{
public:
	FileVersionHelper(std::wstring const& exePath) :
		_exePath(exePath), _strResFetchInitialized(false)
	{
	}

	~FileVersionHelper();
	FileVersionHelper(FileVersionHelper&) = delete;
	FileVersionHelper& operator=(FileVersionHelper&) = delete;

	bool init();
	bool initStrResFetch();
	bool initFixedFileInfo();

	FileVersionInfo const& getFileVersionInfo() const
	{
		return this->_fileVersionInfo;
	}

	wchar_t* getResourceStringVal(std::wstring const& strResName);

private:
	typedef struct
	{
		WORD wLang;
		WORD wCodePage;
	} LANGCODEPAGE, *PLANGCODEPAGE;

	PLANGCODEPAGE _langCodePage;

	std::wstring const& _exePath;
	std::unique_ptr<BYTE[]> _blockData;

	std::wstring _resourcePrefix;

	VS_FIXEDFILEINFO* _versionInfo;
	FileVersionInfo _fileVersionInfo;
	bool _strResFetchInitialized;
};