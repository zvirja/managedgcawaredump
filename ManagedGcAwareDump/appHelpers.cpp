#include "stdafx.h"

using namespace std;

wstring getCurrentDirectory()
{
	const WCHAR DEFAULT_DIR[] = L"C:\\";

	WCHAR buffer[MAX_PATH];
	DWORD retCode;
	if ((retCode = GetModuleFileName(nullptr, buffer, MAX_PATH)) == 0)
	{
		return wstring(DEFAULT_DIR);
	}

	wstring currentModPath(buffer);

	std::wstring dirPath = getDirectoryPath(currentModPath);
	if (dirPath.size() == 0)
	{
		return wstring(DEFAULT_DIR);
	}

	return move(dirPath);
}

std::wstring getDirectoryPath(std::wstring const& filePath)
{
	if (filePath.size() == 0)
		return wstring();

	size_t lastIndOfSeparator = filePath.find_last_of(L"\\/");
	if (wstring::npos == lastIndOfSeparator)
	{
		return wstring();
	}

	return filePath.substr(0, lastIndOfSeparator + 1);
}

std::wstring getNameWithoutExtension(std::wstring const& fileName)
{
	auto lastPosOfDot = fileName.find_last_of(L".");
	if (lastPosOfDot == wstring::npos)
	{
		return fileName;
	}

	return fileName.substr(0, lastPosOfDot);
}

bool fileExits(std::wstring const& filePath)
{
	DWORD dwAttrib = GetFileAttributes(filePath.c_str());
	return dwAttrib != INVALID_FILE_ATTRIBUTES && !(dwAttrib & FILE_ATTRIBUTE_DIRECTORY);
}