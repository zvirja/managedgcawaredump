#pragma once
#include "CollectionTask.h"
#include "xclrdata_h.h"
#include "isosdac_h.h"
#include <clrdata.h>
#include "DebugClrDataTarget.h"
#include "ClrNotificationsReceiver.h"
#include <functional>

class ClrManager
{
public:
	ClrManager(CollectionTask const& collectionTask, DebugClient& debClient) :
		_collectionTask(collectionTask), _debugClient(debClient), _gcResetIsRequired(false),
		_clrNotificationsReceiver([this](ULONG64 gcEventInfo)
			{
				if (_gcEventHandler != nullptr)
				{
					_gcEventHandler(gcEventInfo);
				}
			})
	{
	};

	ClrManager(ClrManager&) = delete;
	ClrManager& operator=(ClrManager&) = delete;
	~ClrManager();

	/**
		Creates basic DAC interfaces. Non-zero if failed.
		*/
	DWORD initializeBasicInterfaces();

	/**
		Returs -1 if error. Otherwise 1 or 0;
		*/
	int isGcHeapValid();

	DWORD initializeExtendedInterfaces(std::function<void(ULONG64)>&& gcEventHandler);

	bool setGcNotificationMode(DWORD32 genMask);
	void resetGcNotifications();

	bool translateNotificationException(PEXCEPTION_RECORD64 exception, ULONG isFirstChance);

private:
	CollectionTask const& _collectionTask;
	DebugClient& _debugClient;
	CComPtr<IXCLRDataProcess> _xClrDataProcess;
	CComPtr<IXCLRDataProcess2> _xClrDataProcess2;
	CComPtr<ISOSDac> _iSOSDac;
	ClrNotificationsReceiver _clrNotificationsReceiver;

	bool _gcResetIsRequired;
	std::function<void(ULONG64)> _gcEventHandler;
};