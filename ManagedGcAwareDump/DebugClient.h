#pragma once
#include <atlbase.h>
#include "CollectionTask.h"
#include "xclrdata_h.h"
#include "isosdac_h.h"

class InterruptableScopeGuard;

enum ExitReason :int
{
	NoExit = 0,
	TargedExited,
	Failure,
	TaskCompleted,
	UnexpectedState,
	UserExitAcquired,
	UserClosedApp,
	SystemCustomGcHappened //Is a temporary state when custom GC was triggered
};

class DebugClient
{
	friend class DebugClrDataTarget;
	friend class IntervalDebugClientRetryer;
	friend class DebugEventCallbacks;
	friend class InterruptableScopeGuard;

	typedef HMODULE(WINAPI * PNtResumeProcess)(HANDLE processHandle);

public:
	DebugClient(ULONG targetPid) :
		_targetProcessId(targetPid), _attachedToProc(false),
		_detachEvent(CreateEvent(nullptr, TRUE, FALSE, L"evDetachFromProcess"))
	{
		this->_appExitReason.store(ExitReason::NoExit);
		_inInterruptableState.store(false);
	}

	DebugClient(DebugClient&) = delete;
	DebugClient(DebugClient&&) = delete;
	DebugClient operator=(DebugClient&) = delete;
	~DebugClient();

	AdvRetCode initClient();

	AdvRetCode attachToProccess();
	void detachFromProccess();
	void acquireExitAsync();
	void acquireExitCritical();
	int tryWithInterval(std::function<bool(int)>&& action, ULONG msecInterval);
	bool createDumpFile(std::wstring const& dumpFileNamePrefix);
	bool enableClrNotificationExceptions(std::function<void(PEXCEPTION_RECORD64, ULONG)>&& handler);
	void executeTargetUntilExitRequested();

	bool getShouldExit() const
	{
		return this->_appExitReason.load(std::memory_order_acquire) != ExitReason::NoExit;
	}

	ExitReason getExitReason() const
	{
		return this->_appExitReason.load();
	}

	void setExitReason(ExitReason reason)
	{
		this->_appExitReason.store(reason, std::memory_order_release);
	}

private:
	CComPtr<IDebugClient> _debugClient;
	CComPtr<IDebugControl> _debugControl;
	CComPtr<IDebugClient2> _debugClient2;
	CComPtr<IDebugSystemObjects> _debugSystemObjects;
	CComPtr<IDebugDataSpaces> _debugDataSpaces;

	ULONG _targetProcessId;
	bool _attachedToProc;
	std::atomic_bool _inInterruptableState;
	std::atomic<ExitReason> _appExitReason;
	std::atomic_bool _memoryDumpIsWritting;
	unique_handle _detachEvent;
	std::function<void(PEXCEPTION_RECORD64, ULONG)> _clrNotificationExceptionsHandler;

	std::wstring getMemoryDumpFileNameWithoutExt(std::wstring const& dumpFileNamePrefix);

	void unSleepTargetProcess();
	void notifyAboutClrNotificationException(PEXCEPTION_RECORD64 exceptionRecord, ULONG firstChance);
	InterruptableScopeGuard enterInterruptableState();

	bool getInInterruptableState() const
	{
		return this->_inInterruptableState.load(std::memory_order_acquire);
	}

	bool interruptIfNeeded();
};

class InterruptableScopeGuard
{
public:
	InterruptableScopeGuard(DebugClient& debClient) :
		_debugClient(debClient), _disposeRequired(true)
	{
		this->_debugClient._inInterruptableState.store(true);
	}

	InterruptableScopeGuard(InterruptableScopeGuard&) = delete;
	InterruptableScopeGuard& operator=(InterruptableScopeGuard&) = delete;

	InterruptableScopeGuard(InterruptableScopeGuard&& other) : InterruptableScopeGuard(other._debugClient)
	{
		other._disposeRequired = false;
	}

	~InterruptableScopeGuard()
	{
		if (this->_disposeRequired)
		{
			this->_debugClient._inInterruptableState.store(false);
		}
	}

private:
	DebugClient& _debugClient;
	bool _disposeRequired;
};