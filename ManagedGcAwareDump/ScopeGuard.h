#pragma once
#include <functional>

class ScopeGuard
{
public:
	ScopeGuard(std::function<void()>&& funcToExecute) :_funcToExecute(move(funcToExecute))
	{
	}

	ScopeGuard(ScopeGuard&) = delete;
	ScopeGuard& operator=(ScopeGuard&) = delete;

	~ScopeGuard()
	{
		try
		{
			if (this->_funcToExecute != nullptr)
			{
				this->_funcToExecute();
			}
		}
		catch (...)
		{
		}
	}

	void cleanup()
	{
		this->_funcToExecute = nullptr;
	}

private:
	std::function<void()> _funcToExecute;
};