#include "stdafx.h"
#include "DebugEventCallbacks.h"


HRESULT DebugEventCallbacks::QueryInterface(IID const& InterfaceId, PVOID* Interface)
{
	if (IsEqualIID(__uuidof(IUnknown), InterfaceId))
	{
		*Interface = this;
		return S_OK;
	}

	if (IsEqualIID(__uuidof(IDebugEventCallbacks), InterfaceId))
	{
		*Interface = static_cast<IDebugEventCallbacks*>(this);
		return S_OK;
	}

	return E_NOINTERFACE;
}

ULONG DebugEventCallbacks::AddRef()
{
	return InterlockedIncrement(&this->_refCount);
}

ULONG DebugEventCallbacks::Release()
{
	LONG oldRef = InterlockedDecrement(&this->_refCount);
	if (this->_refCount == 0)
		delete this;

	return oldRef;
}

HRESULT DebugEventCallbacks::GetInterestMask(PULONG Mask)
{
	*Mask = DEBUG_EVENT_EXCEPTION | DEBUG_EVENT_EXIT_PROCESS;
	return S_OK;
}

HRESULT DebugEventCallbacks::Breakpoint(PDEBUG_BREAKPOINT Bp)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::Exception(PEXCEPTION_RECORD64 Exception, ULONG FirstChance)
{
	//CLR notification exception
	if (Exception->ExceptionCode == CLR_NOTIFICATION_EXCEPTION_CODE)
	{
		this->_debugClient.notifyAboutClrNotificationException(Exception, FirstChance);
		if (this->_debugClient.getShouldExit())
		{
			return DEBUG_STATUS_BREAK;
		}

		return DEBUG_STATUS_GO_HANDLED;
	}

	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::CreateThread(ULONG64 Handle, ULONG64 DataOffset, ULONG64 StartOffset)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::ExitThread(ULONG ExitCode)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::CreateProcessW(ULONG64 ImageFileHandle, ULONG64 Handle, ULONG64 BaseOffset, ULONG ModuleSize, PCSTR ModuleName, PCSTR ImageName, ULONG CheckSum, ULONG TimeDateStamp, ULONG64 InitialThreadHandle, ULONG64 ThreadDataOffset, ULONG64 StartOffset)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::ExitProcess(ULONG ExitCode)
{
	this->_debugClient.setExitReason(ExitReason::TargedExited);
	return DEBUG_STATUS_BREAK;
}

HRESULT DebugEventCallbacks::LoadModule(ULONG64 ImageFileHandle, ULONG64 BaseOffset, ULONG ModuleSize, PCSTR ModuleName, PCSTR ImageName, ULONG CheckSum, ULONG TimeDateStamp)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::UnloadModule(PCSTR ImageBaseName, ULONG64 BaseOffset)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::SystemError(ULONG Error, ULONG Level)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::SessionStatus(ULONG Status)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::ChangeDebuggeeState(ULONG Flags, ULONG64 Argument)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::ChangeEngineState(ULONG Flags, ULONG64 Argument)
{
	return DEBUG_STATUS_GO;
}

HRESULT DebugEventCallbacks::ChangeSymbolState(ULONG Flags, ULONG64 Argument)
{
	return DEBUG_STATUS_GO;
}


DebugEventCallbacks::~DebugEventCallbacks()
{
}