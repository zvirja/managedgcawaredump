#pragma once
#include <clrdata.h>
#include "DebugClient.h"

class DebugClrDataTarget : public ICLRDataTarget2
{
public:
	DebugClrDataTarget(PVOID coreLibBase, DebugClient& debClient) : _refCount(1), _coreLibBase(coreLibBase), _debClient(debClient)
	{
	}

	virtual ~DebugClrDataTarget()
	{
	};

	HRESULT STDMETHODCALLTYPE QueryInterface(IID const& riid, void** ppvObject) override;
	ULONG STDMETHODCALLTYPE AddRef() override;
	ULONG STDMETHODCALLTYPE Release() override;
	HRESULT STDMETHODCALLTYPE GetMachineType(ULONG32* machineType) override;
	HRESULT STDMETHODCALLTYPE GetPointerSize(ULONG32* pointerSize) override;
	HRESULT STDMETHODCALLTYPE GetImageBase(LPCWSTR imagePath, CLRDATA_ADDRESS* baseAddress) override;
	HRESULT STDMETHODCALLTYPE ReadVirtual(CLRDATA_ADDRESS address, BYTE* buffer, ULONG32 bytesRequested, ULONG32* bytesRead) override;
	HRESULT STDMETHODCALLTYPE WriteVirtual(CLRDATA_ADDRESS address, BYTE* buffer, ULONG32 bytesRequested, ULONG32* bytesWritten) override;
	HRESULT STDMETHODCALLTYPE GetTLSValue(ULONG32 threadID, ULONG32 index, CLRDATA_ADDRESS* value) override;
	HRESULT STDMETHODCALLTYPE SetTLSValue(ULONG32 threadID, ULONG32 index, CLRDATA_ADDRESS value) override;
	HRESULT STDMETHODCALLTYPE GetCurrentThreadID(ULONG32* threadID) override;
	HRESULT STDMETHODCALLTYPE GetThreadContext(ULONG32 threadID, ULONG32 contextFlags, ULONG32 contextSize, BYTE* context) override;
	HRESULT STDMETHODCALLTYPE SetThreadContext(ULONG32 threadID, ULONG32 contextSize, BYTE* context) override;
	HRESULT STDMETHODCALLTYPE Request(ULONG32 reqCode, ULONG32 inBufferSize, BYTE* inBuffer, ULONG32 outBufferSize, BYTE* outBuffer) override;
	HRESULT STDMETHODCALLTYPE AllocVirtual(CLRDATA_ADDRESS addr, ULONG32 size, ULONG32 typeFlags, ULONG32 protectFlags, CLRDATA_ADDRESS* virt) override;
	HRESULT STDMETHODCALLTYPE FreeVirtual(CLRDATA_ADDRESS addr, ULONG32 size, ULONG32 typeFlags) override;

private:
	LONG _refCount;
	PVOID _coreLibBase;
	DebugClient& _debClient;
};