#pragma once
#include "CollectionTask.h"
#include "DebugClient.h"
#include "ClrManager.h"

class TaskExecutor
{
	typedef struct _CLIENT_ID
	{
		PVOID UniqueProcess;
		PVOID UniqueThread;
	} CLIENT_ID, *PCLIENT_ID;

	typedef NTSTATUS (NTAPI *_RtlCreateUserThread)(
		IN HANDLE               ProcessHandle,
		IN PSECURITY_DESCRIPTOR SecurityDescriptor OPTIONAL,
		IN BOOLEAN              CreateSuspended,
		IN ULONG                StackZeroBits,
		IN OUT PULONG           StackReserved,
		IN OUT PULONG           StackCommit,
		IN PVOID                StartAddress,
		IN PVOID                StartParameter OPTIONAL,
		OUT PHANDLE             ThreadHandle,
		OUT PCLIENT_ID          ClientID);

public:
	TaskExecutor(CollectionTask& collectionTask, DebugClient& debugClient) :
		_collectionTask(collectionTask), _debugClient(debugClient), _clrManager(collectionTask, debugClient),
		_customGcNotificationExceptionIsExpected(false), _hTargetProc(nullptr), _rtlCreateUserThreadProc(nullptr)
	{
	}

	TaskExecutor(TaskExecutor&) = delete;
	TaskExecutor& operator=(TaskExecutor&) = delete;
	~TaskExecutor();


	void executeTaskAfterAttach();
	bool executeTaskBeforeAttach_PFR();
	void executeTaskAfterDetach_PFR();

	ClrManager* getClrManager()
	{
		return &this->_clrManager;
	}

private:
	CollectionTask& _collectionTask;
	DebugClient& _debugClient;
	ClrManager _clrManager;
	unique_handle _hTargetProc;
	bool _customGcNotificationExceptionIsExpected;
	std::wstring _gcTriggerAssemblyName;
	std::wstring _gcTriggerAssemblyPath;
	BYTE* _gcTriggerAssemblyBaseAddr;
	bool _gcTriggerAssemblyWasHosted;
	_RtlCreateUserThread _rtlCreateUserThreadProc;

	bool initializeDacInterface();
	void createDumpFileWhenGcHeapIsValid();
	bool tryCreateDumpIfHeapValid(int tryNumber);
	bool initGcEventsListening_PFR(bool updateClr);
	void collectDumpOnGcEvents();

	void onNotificationException(PEXCEPTION_RECORD64 exception, ULONG isFirstChance);
	void onGcEvent(ULONG64 gcEventInfo);
	void onRequestedGcEventFinished();


	DWORD32 getNewGcNotificationsMask();
	int getGetFromNotification(ULONG64 gcEventNotification);
	bool setGcNotificationMask_PFR(DWORD32 mask);

	AdvRetCode extractAssemblyFromResources();
	AdvRetCode extractResourceToFile(int resourceID, std::wstring const& outputFileName);

	AdvRetCode hostGcRebuildAssembly();
	int grandEveryoneAccessToAssembly(std::wstring const& filePath);
	bool openTargetProcess();
	void forceGcAndCollectDump();

	AdvRetCode executeInTarget(LPTHREAD_START_ROUTINE routineAddr, LPVOID arg, bool waitForExit = true);
};