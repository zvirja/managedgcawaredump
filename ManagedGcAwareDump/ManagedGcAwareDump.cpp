// ManagedGcAwareDump.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "DebugClient.h"
#include "CommandLineTaskParser.h"
#include "CollectionTask.h"
#include "TaskExecutor.h"
#include "PrematureExiter.h"

#include <io.h>
#include <fcntl.h>

using namespace std;

bool initCtrlCHook();
BOOL WINAPI ctrlCHandlerRoutine(DWORD dwCtrlType);
void printTaskInfo(CollectionTask const& collectionTask);
int printHeader();
void printExitReason(DebugClient const& debClient);
int adjustPrivileges();

PrematureExiter prematureExiter;

int _tmain(int argc, _TCHAR* argv[])
{
#ifdef _UNICODE
	_setmode(_fileno(stdout), _O_U16TEXT);
#endif 

#if defined(DEBUG) || defined(_ADVLOG)
	//To get console delay at the end
	ScopeGuard scGuard([]()
	{
		getchar();
	});
#endif

	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);

	int retCode;
	if ((retCode = printHeader()) != 0)
	{
		wcout << L"Unable to print information header. Error code: " << retCode << endl;
		return 0;
	}

	CommandLineTaskParser cmdParser(argc, argv);
	if (!cmdParser.parse())
	{
		return 0;
	}

	if ((retCode = adjustPrivileges()) != 0)
	{
		wcout << L"** Unable to escalate process privileges. That might cause security errors. Try running app as an administrator. Error code: " << retCode << L" **" << endl << endl;
	}

	CollectionTask collectionTask(cmdParser);
	if (!collectionTask.resolveAndValidateTask())
	{
		return 0;
	}

	printTaskInfo(collectionTask);

	ULONG pidToAttach = collectionTask.getPid();

	DebugClient debugClient(pidToAttach);

	//Init hook only after debug client initialization. If we do that before, it might happen that exit notification will be ignored.
	initCtrlCHook();

	AdvRetCode advRetCode = debugClient.initClient();
	if (advRetCode.isError())
	{
		wcout << L"Unable to initialize application. " << advRetCode << endl;
		return 0;
	}

	TaskExecutor tExecutor(collectionTask, debugClient);
	prematureExiter.setDebugClient(&debugClient);
	prematureExiter.setClrManager(tExecutor.getClrManager());

	//I need that because Exiter lives longer than debClient and clrManager. Otherwise calls to exiter might be performed after inner obj disposal
	ScopeGuard prematureExiterCleaner([](){prematureExiter.cleanup(); });

	ScopeGuard afterDetachCleanup([&tExecutor](){tExecutor.executeTaskAfterDetach_PFR(); });

	if (!tExecutor.executeTaskBeforeAttach_PFR())
	{
		return 0;
	}

	if (prematureExiter.getExitAcquired())
	{
		return 0;
	}

	advRetCode = debugClient.attachToProccess();
	if (advRetCode.isError())
	{
		wcout << L"Unable to attach to process with ID " << pidToAttach << L". " << advRetCode << endl;
		return 0;
	}

	tExecutor.executeTaskAfterAttach();
	debugClient.detachFromProccess();

	printExitReason(debugClient);

	return 0;
}

bool initCtrlCHook()
{
	return SetConsoleCtrlHandler(ctrlCHandlerRoutine, TRUE) == TRUE;
}

BOOL WINAPI ctrlCHandlerRoutine(DWORD dwCtrlType)
{
	if (dwCtrlType == CTRL_C_EVENT)
	{
		wcout << "Ctrl-C hit was trapped. Application will exit in a moment." << endl;
		prematureExiter.acquireRegularExit();
		return true;
	}

	if (dwCtrlType == CTRL_CLOSE_EVENT)
	{
		wcout << L"You are terminating me! I'm trying to cleanup resources.";
		prematureExiter.terminate();
		return false;
	}

	return false;
}

int printHeader()
{
	TCHAR exePath[MAX_PATH];
	GetModuleFileName(nullptr, exePath, MAX_PATH);
	wstring exePathStr(exePath);

	FileVersionHelper fvHelper(exePathStr);
	if (!fvHelper.init())
	{
		return 1;
	}

	if (!(fvHelper.initStrResFetch() && fvHelper.initFixedFileInfo()))
	{
		return 2;
	}

	wchar_t* resVal = fvHelper.getResourceStringVal(L"InternalName");
	if (resVal != nullptr)
	{
		wcout << resVal;
	}

	FileVersionInfo const& fvInfo = fvHelper.getFileVersionInfo();
	wcout << L" - v" << fvInfo.Major << L"." << fvInfo.Minor << L" (" << GetCurrentArchitectureStr() << ")" << endl;

	wcout << L"Developed by Alex Povar." << endl << endl;

	return 0;
}

void printExitReason(DebugClient const& debClient)
{
	auto exitReason = debClient.getExitReason();
	if (exitReason == ExitReason::TargedExited)
	{
		wcout << L"Exiting because the target process has exited." << endl;
	}
	else if (exitReason == ExitReason::UnexpectedState)
	{
		wcout << L"Exiting because of failure during the execution." << endl;
	}
}

int adjustPrivileges()
{
	unique_handle hCurrentProcess = GetCurrentProcess();
	HANDLE hTokenRaw;
	BOOL tokenWasObtained = OpenProcessToken(hCurrentProcess.get(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hTokenRaw);
	unique_handle hToken = hTokenRaw;


	if (!tokenWasObtained)
	{
		return -1;
	}

	LUID luid;

	if (!LookupPrivilegeValue(nullptr, SE_DEBUG_NAME, &luid))
	{
		return -2;
	}

	TOKEN_PRIVILEGES tp;
	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;

	if (!AdjustTokenPrivileges(hToken.get(), false, &tp, sizeof(TOKEN_PRIVILEGES), nullptr, nullptr))
	{
		return -3;
	}

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED)
	{
		return -4;
	}

	return 0;
}

void printTaskInfo(CollectionTask const& collectionTask)
{
	FileVersionInfo const& fileVerInfo = collectionTask.getCoreDllVersionInfo();

	const size_t tempBufferSize = 300;
	unique_ptr<wchar_t[]> tempBuffer = make_unique<wchar_t[]>(tempBufferSize);

	_snwprintf_s(tempBuffer.get(), tempBufferSize, tempBufferSize, L"Target process: %s (%u) .NET %u.%u.%u.%u",
		collectionTask.getProcNameWithoutExt().c_str(), collectionTask.getPid(), fileVerInfo.Major, fileVerInfo.Minor, fileVerInfo.Build, fileVerInfo.Revision);

	wcout << tempBuffer.get() << endl;

	wcout << "Task: ";

	if (collectionTask.isGenSpecificTask())
	{
		wcout << "Collect dump at GC collections:";
		auto& genArr = collectionTask.getGenTasks();
		for (size_t i = 0; i < genArr.size(); ++i)
		{
			int count = genArr[i];
			if (count > 0)
			{
				wcout << " GEN" << i << ":" << count;
			}
		}
	}
	else
	{
		wcout << "Collect dump at first suitable moment.";
	}

	wcout << endl << endl;

	wcout << "Press Ctrl-C to gracefully exit." << endl << endl;
}