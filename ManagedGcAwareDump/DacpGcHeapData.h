#pragma once;
#include "xclrdata_h.h"

#define DACPRIV_REQUEST_GCHEAP_DATA 0xF000002A

struct DacpGcHeapData
{
	BOOL bServerMode;
	BOOL bGcStructuresValid;
	UINT heapCount;
	UINT g_max_generation;

	HRESULT Request(IXCLRDataProcess* dac)
	{
		return dac->Request(DACPRIV_REQUEST_GCHEAP_DATA, 0, nullptr, sizeof(*this), reinterpret_cast<PBYTE>(this));
	}
};