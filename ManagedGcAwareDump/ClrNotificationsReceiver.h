#pragma once
#include "xclrdata_h.h"

class ClrNotificationsReceiver : public IXCLRDataExceptionNotification3
{
public:
	ClrNotificationsReceiver(std::function<void(ULONG64)>&& gcEventHandler) :
		_gcEventHandler(move(gcEventHandler)), _refCount(1)
	{
	}

	ClrNotificationsReceiver(ClrNotificationsReceiver&) = delete;
	ClrNotificationsReceiver& operator=(ClrNotificationsReceiver&) = delete;
	virtual ~ClrNotificationsReceiver();

	HRESULT STDMETHODCALLTYPE QueryInterface(IID const& riid, void** ppvObject) override;
	ULONG STDMETHODCALLTYPE AddRef() override;
	ULONG STDMETHODCALLTYPE Release() override;

	HRESULT STDMETHODCALLTYPE OnCodeGenerated_dummy(BYTE* method) override
	{
		return S_OK;
	};

	HRESULT STDMETHODCALLTYPE OnCodeDiscarded_dummy(BYTE* method) override
	{
		return S_OK;
	};

	HRESULT STDMETHODCALLTYPE OnProcessExecution(ULONG32 state) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnTaskExecution_dummy(BYTE* task, ULONG32 state) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnModuleLoaded_dummy(BYTE* mod) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnModuleUnloaded_dummy(BYTE* mod) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnTypeLoaded_dummy(BYTE* typeInst) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnTypeUnloaded_dummy(BYTE* typeInst) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnAppDomainLoaded_dummy(BYTE* domain) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnAppDomainUnloaded_dummy(BYTE* domain) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnException_dummy(BYTE* exception) override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE OnGcEvent(ULONG64 eventInfo) override;

	HRESULT STDMETHODCALLTYPE Unknown1_dummmy() override
	{
		return S_OK;
	}

	HRESULT STDMETHODCALLTYPE Unknown2_dummmy() override
	{
		return S_OK;
	}

private:
	LONG _refCount;
	std::function<void(ULONG64)> _gcEventHandler;
};