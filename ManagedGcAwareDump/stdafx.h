// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>

//STD
#include <atlbase.h>
#include <memory>
#include <iostream>
#include <string>
#include <sstream>
#include <cwchar>
#include <atomic>
#include <thread>
#include <mutex>
#include <algorithm>
#include <vector>
#include <array>

//#include <chrono>
#include <ctime>
#include <iomanip>

//WINAPI
#include <Dbgeng.h>
#include <TlHelp32.h>

//Current project
#include "appHelpers.h"
#include "unique_handle.h"
#include "FileVersionHelper.h"
#include "ScopeGuard.h"

/*#define _GCRUNNERTEST 1*/
/*#define _ADVLOG 1*/