#pragma once
#include "Windows.h"

class unique_handle
{
public:
	unique_handle(HANDLE h) : _handle(h)
	{
	};

	unique_handle(unique_handle&) = delete;

	unique_handle& operator=(unique_handle&) = delete;

	unique_handle& operator=(unique_handle&& other)
	{
		this->_handle = other._handle;
		other.preventReclaim();
		return *this;
	}

	unique_handle& operator=(HANDLE handle)
	{
		this->_handle = handle;
		return *this;
	}

	~unique_handle()
	{
		if (this->isValid())
		{
			CloseHandle(this->_handle);
			this->_handle = nullptr;
		}
	}

	HANDLE get() const
	{
		return this->_handle;
	};

	bool isValid() const
	{
		return this->_handle != nullptr && this->_handle != INVALID_HANDLE_VALUE;
	}

	void preventReclaim()
	{
		this->_handle = nullptr;
	}

private:
	HANDLE _handle;
};