#include "stdafx.h"
#include "DebugClient.h"
#include "TlHelp32.h"
#include "DacpGcHeapData.h"
#include "DebugEventCallbacks.h"
#include "IntervalDebugClientRetryer.h"

using namespace std;

DebugClient::~DebugClient()
{
	if (this->_attachedToProc)
	{
		this->detachFromProccess();
	}
}

AdvRetCode DebugClient::initClient()
{

	if (this->getShouldExit())
	{
		return AdvRetCode(-1);
	}

	HRESULT status;
	if ((status = DebugCreate(__uuidof(IDebugClient), reinterpret_cast<PVOID*>(&(this->_debugClient)))) != S_OK)
	{
		return AdvRetCode(1, status);
	}

	if ((status = this->_debugClient->QueryInterface(__uuidof(IDebugClient2), reinterpret_cast<PVOID*>(&(this->_debugClient2)))) != S_OK)
	{
		return AdvRetCode(2, status);
	}

	if ((status = this->_debugClient->QueryInterface(__uuidof(IDebugControl), reinterpret_cast<PVOID*>(&(this->_debugControl)))) != S_OK)
	{
		return AdvRetCode(3, status);
	}

	if ((status = this->_debugControl->AddEngineOptions(DEBUG_ENGOPT_INITIAL_BREAK)) != S_OK)
	{
		return AdvRetCode(4, status);
	}

	if ((status = this->_debugClient->QueryInterface(__uuidof(IDebugSystemObjects), reinterpret_cast<PVOID*>(&(this->_debugSystemObjects)))) != S_OK)
	{
		return AdvRetCode(5, status);
	}

	if ((status = this->_debugClient->QueryInterface(__uuidof(IDebugDataSpaces), reinterpret_cast<PVOID*>(&(this->_debugDataSpaces)))) != S_OK)
	{
		return AdvRetCode(6, status);
	}

	if (this->_detachEvent.get() == nullptr)
	{
		return AdvRetCode(7);
	}

	return AdvRetCode();
}

AdvRetCode DebugClient::attachToProccess()
{
	if (this->getShouldExit())
	{
		return AdvRetCode(-1);
	}

	ULONG pid = this->_targetProcessId;
	if (this->_attachedToProc)
	{
		return AdvRetCode(1);
	}

	HRESULT status = 0;
	if ((status = this->_debugClient->AttachProcess(0, pid, DEBUG_ATTACH_DEFAULT)) != S_OK)
	{
		return AdvRetCode(2, status);
	}

	if (this->_debugControl->WaitForEvent(DEBUG_WAIT_DEFAULT, 10000) == S_FALSE)
	{
		return AdvRetCode(3);
	}

	this->_attachedToProc = true;

	if ((status = this->_debugClient->AddProcessOptions(DEBUG_PROCESS_DETACH_ON_EXIT)) != S_OK)
	{
		return AdvRetCode(4, status);
	}

	return AdvRetCode();
}

bool DebugClient::enableClrNotificationExceptions(std::function<void(PEXCEPTION_RECORD64, ULONG)>&& handler)
{
	this->_clrNotificationExceptionsHandler = handler;

	unique_ptr<DebugEventCallbacks> debEventCallback = make_unique<DebugEventCallbacks>(*this);
	HRESULT status = this->_debugClient->SetEventCallbacks(debEventCallback.get());

	debEventCallback->Release();
	debEventCallback.release();

	return status == S_OK;
}

void DebugClient::executeTargetUntilExitRequested()
{
	HRESULT status = 0;
	do
	{
		if (this->getShouldExit())
		{
			break;
		}

		this->_debugControl->SetExecutionStatus(DEBUG_STATUS_GO);

		{
			auto interruptableState = this->enterInterruptableState();
			status = this->_debugControl->WaitForEvent(0, INFINITE);
		}
	} while (status == S_OK);
}

void DebugClient::notifyAboutClrNotificationException(PEXCEPTION_RECORD64 exceptionRecord, ULONG firstChance)
{
	if (this->_clrNotificationExceptionsHandler != nullptr)
	{
		this->_clrNotificationExceptionsHandler(exceptionRecord, firstChance);
	}
	else
	{
		this->setExitReason(ExitReason::UnexpectedState);
	}
}

InterruptableScopeGuard DebugClient::enterInterruptableState()
{
	return InterruptableScopeGuard(*this);
}

bool DebugClient::interruptIfNeeded()
{
	if (!this->getInInterruptableState())
	{
		return false;
	}

	return this->_debugControl->SetInterrupt(DEBUG_INTERRUPT_ACTIVE) == S_OK;
}

void DebugClient::detachFromProccess()
{
	if (this->_attachedToProc)
	{
		//No sense to perform detach for exiting process
		if (this->getExitReason() != ExitReason::TargedExited)
		{
			this->_debugClient->EndSession(DEBUG_END_ACTIVE_DETACH);
			this->_debugClient->DetachProcesses();
		}

		this->_attachedToProc = false;
	}

	SetEvent(this->_detachEvent.get());
	wcout << endl << L"Detached from the target process." << endl;
}

void DebugClient::acquireExitAsync()
{
	this->setExitReason(ExitReason::UserExitAcquired);
	this->interruptIfNeeded();
}

void DebugClient::acquireExitCritical()
{
	this->setExitReason(ExitReason::UserClosedApp);

	if (!this->_attachedToProc)
	{
		return;
	}

	if (this->_memoryDumpIsWritting.load(memory_order_acquire))
	{
		wcout << "The target is prematurely unfrozed. The created dump might be invalid!" << endl;
		this->unSleepTargetProcess();
	}
	else
	{
		this->interruptIfNeeded();
		WaitForSingleObject(this->_detachEvent.get(), 1000);
	}
}

int DebugClient::tryWithInterval(std::function<bool(int)>&& action, ULONG msecInterval)
{
	//Here I should perform deferred gathering.
	IntervalDebugClientRetryer retryer(move(action), 250, *this);
	int status = retryer.TryUntilSuccessOrFail();

	return status;
}

bool DebugClient::createDumpFile(std::wstring const& dumpFileNamePrefix)
{
	wstring curDir = getCurrentDirectory();
	wstring memoryDumpFileName = this->getMemoryDumpFileNameWithoutExt(dumpFileNamePrefix);

	wstring fullPath(curDir + memoryDumpFileName + L".dmp");

	while (fileExits(fullPath))
	{
		memoryDumpFileName += L"_" + to_wstring(GetTickCount64());
		fullPath = curDir + memoryDumpFileName + L".dmp";
	}

	//Flag should be set before we check exit flag.
	//That will allow to thread safely set exit reason and check whether process's threads resume is required.
	this->_memoryDumpIsWritting.store(true, memory_order_release);
	ScopeGuard resetDumpIsWriting([this]()
	{
		_memoryDumpIsWritting.store(false, memory_order_release);
	});


	//it isn't allowed to create dump if app should exit
	if (this->getShouldExit())
	{
		return false;
	}

	memoryDumpFileName += L".dmp";

	wcout << L"Writing memory dump with name " << memoryDumpFileName << L"..." << endl;

	string nonWideString(memoryDumpFileName.begin(), memoryDumpFileName.end());

	HRESULT retCode;

	WCHAR oldDir[MAX_PATH];
	bool oldDirStoredSuccessfully = GetCurrentDirectory(MAX_PATH, oldDir) != 0;

	ScopeGuard resetCurrentDir([&oldDir]()
	{
		SetCurrentDirectory(oldDir);
	});
	if (oldDirStoredSuccessfully)
	{
		SetCurrentDirectory(curDir.c_str());
	}
	else
	{
		resetCurrentDir.cleanup();
	}


	//	if ((retCode = this->_debugClient5->WriteDumpFileWide(fullPath.data(), 0, DEBUG_DUMP_DEFAULT, 0, nullptr)) != S_OK)
	if ((retCode = this->_debugClient2->WriteDumpFile2(nonWideString.c_str(), DEBUG_DUMP_SMALL,
		DEBUG_FORMAT_USER_SMALL_FULL_MEMORY | DEBUG_FORMAT_USER_SMALL_FULL_MEMORY_INFO | DEBUG_FORMAT_USER_SMALL_HANDLE_DATA | DEBUG_FORMAT_USER_SMALL_UNLOADED_MODULES | DEBUG_FORMAT_USER_SMALL_THREAD_INFO,
		"Created using the ManagedGcAwareDump tool.")) != S_OK)
	{
		wcout << L"Failed to write dump file. Error code: " << hex << retCode << dec << endl;
		return false;
	}

	wcout << L"Memory dump file has been successfully written." << endl;

	return true;
}

void DebugClient::unSleepTargetProcess()
{
	HMODULE hModule = GetModuleHandle(L"ntdll.dll");
	PNtResumeProcess resProc = reinterpret_cast<PNtResumeProcess>(GetProcAddress(hModule, "NtResumeProcess"));
	if (resProc != nullptr)
	{
		unique_handle hTargetProc = OpenProcess(PROCESS_SUSPEND_RESUME, FALSE, this->_targetProcessId);
		if (hTargetProc.isValid())
		{
			resProc(hTargetProc.get());
		}
	}
}

std::wstring DebugClient::getMemoryDumpFileNameWithoutExt(wstring const& dumpFileNamePrefix)
{
	wstringstream result;

	if (dumpFileNamePrefix.size() > 0)
	{
		result << dumpFileNamePrefix << L"_";
	}

	time_t now;
	time(&now);
	tm nowTm;
	localtime_s(&nowTm, &now);
	result << std::put_time(&nowTm, L"%Y-%m-%d_%H-%M-%S");

	return result.str();
}