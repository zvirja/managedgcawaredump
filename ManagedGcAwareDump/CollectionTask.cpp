#include "stdafx.h"
#include "CollectionTask.h"
#include <TlHelp32.h>
#include <wchar.h>

using namespace std;

CollectionTask::~CollectionTask()
{
}

bool CollectionTask::resolveAndValidateTask()
{
	if (!this->validateFlagsConsistency())
	{
		return false;
	}

	if (!this->ensureProcessIdAndName())
	{
		return false;
	}

	if (!this->validateBitness())
	{
		return false;
	}

	if (!this->resolveCoreClrDll())
	{
		return false;
	}

	if (!this->initCoreDllVersionInfo())
	{
		return false;
	}

	if (!this->ensureDotNetVersionForOnGc())
	{
		return false;
	}

	if (!this->validateRedistrInstalled())
	{
		return false;
	}

	return true;
}

bool CollectionTask::ensureProcessIdAndName()
{
	unique_handle hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapshot.get() == INVALID_HANDLE_VALUE)
	{
		wcout << L"Unable to enumerate running processes." << endl;
		return false;
	}

	ULONG pid = 0;
	wstring pName;
	if (this->_cmdParser.resolveByProcId())
	{
		pid = this->_cmdParser.getProcId();
	}
	else
	{
		//prepare name
		wstring const& specifiedName = this->_cmdParser.getProcName();
		pName.resize(specifiedName.size());
		transform(specifiedName.begin(), specifiedName.end(), pName.begin(), tolower);

		wstring exeSuffix(L".exe");
		if (pName.size() < exeSuffix.size() || !equal(exeSuffix.rbegin(), exeSuffix.rend(), pName.rbegin()))
		{
			pName.append(exeSuffix);
		}
	}

	//Find process with appropriate ID or Name

	PROCESSENTRY32 pEntry;
	pEntry.dwSize = sizeof(pEntry);

	if (!Process32First(hSnapshot.get(), &pEntry))
	{
		wcout << L"Error during running processes enumeration." << endl;
		return false;
	}

	vector<ULONG> procIdsForProcByName;

	do
	{
		//Id mode
		if (pid != 0)
		{
			if (pEntry.th32ProcessID == pid)
			{
				this->_pid = pid;
				this->_procNameWithoutExt = getNameWithoutExtension(wstring(pEntry.szExeFile));
				return true;
			}
		}
		//nameMode
		else
		{
			//Check if name equals with expected
			if (_wcsnicmp(pEntry.szExeFile, pName.data(), pName.size()) == 0)
			{
				//first time we are here
				if (procIdsForProcByName.size() == 0)
				{
					this->_procNameWithoutExt = getNameWithoutExtension(wstring(pEntry.szExeFile));
				}
				procIdsForProcByName.push_back(pEntry.th32ProcessID);
			}
		}
	}
	while (Process32Next(hSnapshot.get(), &pEntry));

	// If we are here for PID mode - process with such ID was not found.
	if (pid != 0)
	{
		wcout << L"Process with ID " << pid << L" was not found. Ensure that valid process ID is specified." << endl;
		return false;
	}

	//We are here because we searched by process name. Need to validate results
	if (procIdsForProcByName.size() == 0)
	{
		wcout << L"Unable to find process with name '" << this->_cmdParser.getProcName() << L"'. Ensure that valid process name is specified." << endl;
		return false;
	}

	if (procIdsForProcByName.size() > 1)
	{
		wcout << L"A few processes with name '" << this->_cmdParser.getProcName() << "' were found. Specify process by ID. Consider one of the following PIDs:" << endl;
		bool first = true;
		for (ULONG id : procIdsForProcByName)
		{
			if (!first)
			{
				wcout << L" | ";
			}
			else
			{
				first = false;
			}

			wcout << id;
		}
		wcout << endl;
		return false;
	}

	//We are here because process was specified by name and we have found only one id.
	this->_pid = procIdsForProcByName.at(0);

	return true;
}

bool CollectionTask::validateBitness()
{
	unique_handle hTargetProc = OpenProcess(PROCESS_QUERY_INFORMATION, FALSE, this->getPid());

	if (!hTargetProc.isValid())
	{
		wcout << L"Unable to obtain handle to the target process. Error code: " << GetLastError() << ". Try to run app as an administrator." << endl;
		return false;
	}

	int bitnessValidationResult = this->bitnessMatch(hTargetProc);

	if (bitnessValidationResult < 0)
	{
		wcout << L"Error during target proces bitness validation. Error code: " << bitnessValidationResult << endl;
		return false;
	}

	if (bitnessValidationResult == 0)
	{
#if _WIN64
		wchar_t validArch[] = L"x64";
		wchar_t wrongArch[] = L"x86";
#else
		wchar_t validArch[] = L"x86";
		wchar_t wrongArch[] = L"x64";
#endif 
		wcout << L"Target process bitness is " << wrongArch << L" while " << validArch << L" processes could be handled only." << endl << L"Use " << wrongArch << " version of tool to create memory dump file." << endl;
		return false;
	}

	return true;
}

bool CollectionTask::resolveCoreClrDll()
{
	unique_handle hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, this->getPid());
	if (!hSnapshot.isValid())
	{
		wcout << L"Unable to enumerate modules for process with ID " << this->getPid() << endl;
		return false;
	}

	MODULEENTRY32 mod_entry;
	mod_entry.dwSize = sizeof(mod_entry);

	if (Module32First(hSnapshot.get(), &mod_entry))
	{
		// iterate through the module list of the process
		do
		{
			if (wcscmp(mod_entry.szModule, L"clr.dll") == 0 || wcscmp(mod_entry.szModule, L"mscorwks.dll") == 0)
			{
				this->_coreDllAddress = static_cast<PVOID>(mod_entry.modBaseAddr);
				this->_coreDllPath = wstring(mod_entry.szExePath);
				return true;
			}
		}
		while (Module32Next(hSnapshot.get(), &mod_entry));
	}

	wcout << L"Process " << this->getProcNameWithoutExt() << L"(" << this->getPid() << L") is not a valid .NET process (it contains neither clr.dll or mscorwks.dll library)." << endl;
	return false;
}

int CollectionTask::bitnessMatch(unique_handle const& hTargetProc)
{
	BOOL isCurrentWow;
	if (IsWow64Process(GetCurrentProcess(), &isCurrentWow) != TRUE)
	{
		return -2;
	}

	BOOL isOtherWow;
	if (IsWow64Process(hTargetProc.get(), &isOtherWow) != TRUE)
	{
		return -3;
	}

#if _WIN64
	if (isCurrentWow == TRUE)
	{
		return -4;
	}
	return isOtherWow == TRUE ? 0 : 1;
#else
	//pure x86 environment
	if (isCurrentWow == FALSE)
	{
		return 1;
	}
	return isOtherWow == TRUE ? 1 : 0;
#endif

	return -1;
}

bool CollectionTask::initCoreDllVersionInfo()
{
	this->_coreDllFileVerHelper = make_unique<FileVersionHelper>(this->getCoreDllPath());
	if (this->_coreDllFileVerHelper->init() && this->_coreDllFileVerHelper->initFixedFileInfo())
	{
		return true;
	}

	wcout << L"Unable to get information about core CLR library version." << endl;

	return false;
}

bool CollectionTask::ensureDotNetVersionForOnGc()
{
	if (!this->isGenSpecificTask() && !this->shouldForceGcBeforeCollection())
	{
		return true;
	}

	//is suitable for .NET 4.0 and higher only.
	if (this->getCoreDllVersionInfo().Major >= 4)
	{
		return true;
	}

	wcout << L"The required feature is available for .NET 4.0 and higher only." << endl;
	return false;
}

bool CollectionTask::validateFlagsConsistency()
{
	if (this->isGenSpecificTask() && this->shouldForceGcBeforeCollection())
	{
		wcout << L"/g and /fgc flags are incompatible with each other. Ensure that only one flag is specified simultaneously." << endl;
		return false;
	}

	if (this->waitForPendingFinalizers() && !this->shouldForceGcBeforeCollection())
	{
		wcout << L"/wpf flag could be specified if the /fgc flag is specified only." << endl;
		return false;
	}

	return true;
}

bool CollectionTask::validateRedistrInstalled()
{
	if (!this->shouldForceGcBeforeCollection())
	{
		return true;
	}

	HMODULE hLib = LoadLibrary(L"msvcr110.dll");
	if (hLib == nullptr)
	{
		wcout << L"The \"Force GC collection\" functionality requires the Visual C++ Redistributable Package 2012 " << GetCurrentArchitectureStr() << L" to be installed.\r\nYou can download package from the official Microsoft site." << endl;
		return false;
	}

	FreeLibrary(hLib);
	return true;
}