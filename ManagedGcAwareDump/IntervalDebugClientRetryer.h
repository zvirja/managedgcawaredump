#pragma once
#include <functional>
#include <atomic>

class DebugClient;

class IntervalDebugClientRetryer
{
public:
	IntervalDebugClientRetryer(std::function<bool(int)>&& func, ULONG delay, DebugClient& debClient) :
		_func(std::move(func)), _delay(delay), _debClient(debClient)
	{
		this->_bgThreadRequired.store(true);
		this->_nextFireTime.store(0);
	};

	IntervalDebugClientRetryer(IntervalDebugClientRetryer&) = delete;
	IntervalDebugClientRetryer(IntervalDebugClientRetryer&&) = delete;
	IntervalDebugClientRetryer& operator=(IntervalDebugClientRetryer&) = delete;

	~IntervalDebugClientRetryer()
	{
	}

	int TryUntilSuccessOrFail();
private:
	std::function<bool(int)> _func;
	ULONG _delay;
	DebugClient& _debClient;
	std::atomic_bool _bgThreadRequired;
	std::atomic<ULONGLONG> _nextFireTime;

	bool startBgWakeupThread();
	void bgWakeupThreadWorkLoop();
};