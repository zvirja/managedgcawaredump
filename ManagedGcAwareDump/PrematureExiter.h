#pragma once
#include "DebugClient.h"
#include "ClrManager.h"

class PrematureExiter
{
public:
	PrematureExiter()
	{
	}

	~PrematureExiter()
	{
		if (this->_debugClient != nullptr)
		{
			this->terminate();
		}
	}

	void acquireRegularExit()
	{
		this->setExitWasAcquired();
		if (this->_debugClient != nullptr)
		{
			this->_debugClient->acquireExitAsync();
		}
		this->cleanup();
	}

	void terminate()
	{
		this->setExitWasAcquired();
		if (this->_debugClient != nullptr)
		{
			this->_debugClient->acquireExitCritical();
		}
		if (this->_clrManager != nullptr)
		{
			this->_clrManager->resetGcNotifications();
		}
		this->cleanup();
	}

	void cleanup()
	{
		this->_debugClient = nullptr;
		this->_clrManager = nullptr;
	}

	void setDebugClient(DebugClient* debClient)
	{
		this->_debugClient = debClient;
	}

	void setClrManager(ClrManager* clrManager)
	{
		this->_clrManager = clrManager;
	}

	bool getExitAcquired()
	{
		return this->_exitAcquired.load(std::memory_order_acquire);
	}

private:
	DebugClient* _debugClient;
	ClrManager* _clrManager;
	std::atomic<bool> _exitAcquired;

	void setExitWasAcquired()
	{
		this->_exitAcquired.store(true, std::memory_order_release);
	}
};