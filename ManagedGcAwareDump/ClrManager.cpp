#include "stdafx.h"
#include "ClrManager.h"
#include "DacpGcHeapData.h"

using namespace std;


ClrManager::~ClrManager()
{
}

DWORD ClrManager::initializeBasicInterfaces()
{
	std::wstring coreDirPath = getDirectoryPath(this->_collectionTask.getCoreDllPath());
	if (coreDirPath.size() == 0)
	{
		return 1;
	}

	wstring dacLibPath = coreDirPath + L"mscordacwks.dll";

	HMODULE hDacLib = LoadLibrary(dacLibPath.c_str());
	if (hDacLib == nullptr)
	{
		return 2;
	}

	PFN_CLRDataCreateInstance entry = reinterpret_cast<PFN_CLRDataCreateInstance>(GetProcAddress(hDacLib, "CLRDataCreateInstance"));

	if (entry == nullptr)
	{
		FreeLibrary(hDacLib);
		return 3;
	}

	unique_ptr<DebugClrDataTarget> dataTarget = make_unique<DebugClrDataTarget>(this->_collectionTask.getCoreDllAddress(), this->_debugClient);

	HRESULT status;
	if ((status = entry(__uuidof(IXCLRDataProcess), dataTarget.get(), reinterpret_cast<PVOID*>(&(this->_xClrDataProcess)))) != S_OK)
	{
		FreeLibrary(hDacLib);
		return status;
	}

	if ((status = this->_xClrDataProcess->QueryInterface(__uuidof(ISOSDac), reinterpret_cast<PVOID*>(&(this->_iSOSDac)))) != S_OK)
	{
		//_iSOSDac will hold nullRef
		this->_iSOSDac.Attach(nullptr);
	}

	//Decrement ref count
	dataTarget->Release();
	dataTarget.release();

	return 0;
}

int ClrManager::isGcHeapValid()
{
	DacpGcHeapData heapData;

	HRESULT status;
	this->_xClrDataProcess->Flush();

	if (this->_iSOSDac == nullptr)
	{
		status = heapData.Request(this->_xClrDataProcess);
	}
	else
	{
		status = this->_iSOSDac->GetGCHeapData(reinterpret_cast<BYTE*>(&heapData));
	}

	if (status != S_OK)
	{
		return -1;
	}

	return heapData.bGcStructuresValid;
}

DWORD ClrManager::initializeExtendedInterfaces(std::function<void(ULONG64)>&& gcEventHandler)
{
	if (this->_xClrDataProcess->QueryInterface(__uuidof(IXCLRDataProcess2), reinterpret_cast<PVOID*>(&(this->_xClrDataProcess2))) != S_OK)
	{
		this->_xClrDataProcess2.Attach(nullptr);
		return 100;
	}

	this->_gcEventHandler = move(gcEventHandler);

	return 0;
}

bool ClrManager::setGcNotificationMode(DWORD32 genMask)
{
	if (this->_xClrDataProcess2 == nullptr)
	{
		return false;
	}

	ULONG64 gcNotificationFlags = 1;
	gcNotificationFlags |= (static_cast<ULONG64>(genMask) << 32);

	HRESULT status = this->_xClrDataProcess2->SetGcNotification(gcNotificationFlags);

	if (status == S_OK)
	{
		//we have updated GC notification mode
		this->_gcResetIsRequired = genMask != 0;
		return true;
	}

	return false;
}

void ClrManager::resetGcNotifications()
{
	if (this->_gcResetIsRequired)
	{
		this->setGcNotificationMode(0);
	}
}

bool ClrManager::translateNotificationException(PEXCEPTION_RECORD64 exception, ULONG isFirstChance)
{
	return this->_xClrDataProcess->TranslateExceptionRecordToNotification(exception, &this->_clrNotificationsReceiver) == S_OK;
}