#include "stdafx.h"
#include "FileVersionHelper.h"

using namespace std;

FileVersionHelper::~FileVersionHelper()
{
}

bool FileVersionHelper::init()
{
	DWORD dummyBuffer;
	DWORD bufferSize = GetFileVersionInfoSize(this->_exePath.c_str(), &dummyBuffer);
	if (bufferSize == 0)
	{
		return false;
	}

	this->_blockData = make_unique<BYTE[]>(bufferSize);
	if (GetFileVersionInfo(this->_exePath.c_str(), dummyBuffer, bufferSize, this->_blockData.get()) == 0)
	{
		return false;
	}


	return true;
}

bool FileVersionHelper::initStrResFetch()
{
	UINT cbTranslate;
	if (VerQueryValue(this->_blockData.get(), L"\\VarFileInfo\\Translation", reinterpret_cast<PVOID*>(&this->_langCodePage), &cbTranslate) == 0)
	{
		return false;
	}

	wchar_t swKey[40];
	if (swprintf(swKey, sizeof(swKey), L"\\StringFileInfo\\%04X%04X\\", this->_langCodePage[0].wLang, this->_langCodePage[0].wCodePage) < 0)
	{
		return false;
	}

	this->_resourcePrefix = wstring(swKey);
	this->_strResFetchInitialized = true;

	return true;
}

bool FileVersionHelper::initFixedFileInfo()
{
	unsigned dwBytes;
	if (VerQueryValue(this->_blockData.get(), L"\\", reinterpret_cast<PVOID*>(&this->_versionInfo), &dwBytes) == 0)
	{
		return false;
	}

	if (dwBytes == 0)
	{
		return false;
	}

	if (this->_versionInfo->dwSignature != 0xfeef04bd)
	{
		return false;
	}

	this->_fileVersionInfo.Major = HIWORD(this->_versionInfo->dwFileVersionMS);
	this->_fileVersionInfo.Minor = LOWORD(this->_versionInfo->dwFileVersionMS);
	this->_fileVersionInfo.Build = HIWORD(this->_versionInfo->dwFileVersionLS);
	this->_fileVersionInfo.Revision = LOWORD(this->_versionInfo->dwFileVersionLS);

	return true;
}

wchar_t* FileVersionHelper::getResourceStringVal(wstring const& strResName)
{
	if (!_strResFetchInitialized && !this->initStrResFetch())
	{
		return nullptr;
	}

	wchar_t* valueBuf;
	UINT dwBytes;

	wstring key = this->_resourcePrefix + strResName;

	if (VerQueryValue(this->_blockData.get(), key.c_str(), reinterpret_cast<PVOID*>(&valueBuf), &dwBytes) == 0)
	{
		return nullptr;
	}

	return valueBuf;
}