Managed Gc Aware Dump
===================

Latest version: 1.0

Quick description
=================

ManagedGcAwareDump tool allows to collect memory dump file at GC aware points of time. Tool guarantees that heap in memory dump file is always in consistent state (traversable). Tool support a few different modes, e.g. allowing you to force GC immediately before the memory dump file gathering.

Refer to the [Wiki](/zvirja/managedgcawaredump/wiki/Home) to get insight into the supported options.

Download link:
==============
[https://bitbucket.org/zvirja/managedgcawaredump-binaries](https://bitbucket.org/zvirja/managedgcawaredump-binaries)