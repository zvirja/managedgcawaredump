
#include "DllLifecycle.h"

BOOL WINAPI DllMain(	_In_  HINSTANCE hinstDLL,	_In_  DWORD fdwReason,	_In_  LPVOID lpvReserved	)
{
	switch (fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		hForceGcModule = reinterpret_cast<HMODULE>(hinstDLL);
		break;
	}

	return TRUE;
}


DWORD WINAPI UnloadModule(_In_  LPVOID lpParameter)
{
	FreeLibraryAndExitThread(hForceGcModule, 0);
	return 0;
}

