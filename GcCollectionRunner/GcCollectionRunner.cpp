#include "GcCollectionRunner.h"

void DoForcedGc(bool waitForPendingFinalizers)
{
	try
	{
		System::GC::Collect();
		if (waitForPendingFinalizers)
		{
			System::GC::WaitForPendingFinalizers();
		}
	}
	catch (System::Exception^)
	{
		
	}
}

void DoForcedGcGen(int gen, bool waitForPendingFinalizers)
{
	try
	{
		System::GC::Collect(gen, System::GCCollectionMode::Forced);
		if (waitForPendingFinalizers)
		{
			System::GC::WaitForPendingFinalizers();
		}
	}
	catch (System::Exception^)
	{

	}
}