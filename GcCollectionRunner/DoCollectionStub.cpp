#include "GcCollectionRunner.h"
#include <windows.h>

extern "C"
__declspec(dllexport) DWORD WINAPI DoForcedGcStub(_In_  LPVOID lpParameter)
{
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);

	DWORD32 dwParam = reinterpret_cast<DWORD32>(lpParameter);

	bool waitForPendingFinalizers = (dwParam & 0x100) != 0;

	if ((dwParam & 0x10) == 0)
	{
		DoForcedGc(waitForPendingFinalizers);
	}
	else
	{
		DWORD32 genVal = dwParam & 3;
		int genValInt = static_cast<int>(genVal);
		DoForcedGcGen(genValInt, waitForPendingFinalizers);
	}

	try
	{
		ULONG_PTR exceptionArguments[1];
		exceptionArguments[0] = 0xdeadf00d;
		const DWORD clrNotificationException = 0xE0444143;

		//Raise notification exception
		RaiseException(clrNotificationException, 0, 1, exceptionArguments);
	}
	catch (...)
	{

	}
	return 0;
}